<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\member;
class membercontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view ('ticket');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mem = new member
         (['email'=>$request->get ('email'),
            'passwd'=>$request->get ('passwd'),
            'firstname'=>$request->get ('firstname'),
        'lastname'=>$request->get ('lastname')
        ,
        'telnumber'=>$request->get ('telnumber')

    ]) ;
        $mem -> save ();
        return view ('ticket');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('user')->where('id',$id)->get();
        return view('edit',['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
        {
            $data = $request->all();
            $id = $data['id'];
            $result = array(
                'email' => $data['email'],
                'passwd' => $data['passwd'],
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
            );
            DB::table('members')->where('id',$id)->update($result);
                    return redirect('edit');
             }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class Controllernaja extends Controller
{
    public function homes(){
    	return view ('homes');
    }
    public function ticket(){
    	return view ('ticket');
    }
    public function schedule(){
    	return view ('schedule');
    }
    public function singin() {
    	return view ('singin');
    }    
    public function register() {
        return view ('register');
    } 
    public function chek_login(){
        return view ('chek_login');
    }
    public function shop(){
        return view ('shop');
    }
    public function shopdetail(){
        return view ('shopdetail');
    }
    public function edit(){
        return view ('edit');
    }
    public function shopsingin(){
        return view ('shopsingin');
    }
    public function admin(){
        return view ('admin');
    }
    public function ordering(){
        return view ('ordering');
    }



    public function chekregister(Request $request){
        $data = $request->all();
        DB::table('members')->insert(
            [
            'email' => $data['email'],
            'passwd' => $data['passwd'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'telnumber' => $data['telnumber']
            ]);    
            return redirect('ticket');
    }
    public function checkLogin(Request $request){

        $data = $request->all();         
        $user = $data['email'];
        $pass = $data['passwd'];
        
        $array = array(
            'email' => $user,
            'passwd' => $pass
        );
        $ans = DB::table('members')->where($array)->get();
        $row_count = count($ans);
        
        if($row_count == 1){
            session(['id' => $ans[0]->id]);
            session(['name' => $ans[0]->firstname.' '.$ans[0]->lastname]);
            session(['firstname' => $ans[0]->firstname]);
            session(['email' => $ans[0]->email]);
            session(['lastname' => $ans[0]->lastname]);
            session(['telnumber' => $ans[0]->telnumber]); 
            session(['passwd' => $ans[0]->passwd]);
            return redirect('ticket');
            }
            
        else{
            return redirect('/singin');
        }
    }
    public function logout (){
        session_start();
        session_destroy();

        session()->flush();

        return redirect('/ticket');


    }

    public function update(Request $request)
        {
            $data = $request->all();
            $id = $data['id'];
            $result = array(
                'email' => $data['email'],
                'passwd' => $data['passwd'],
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
            );
            DB::table('members')->where('id',$id)->update($result);            
                    return view('ticket');
             }


}


$(document).on("click",".selectMem",function() {
    $('#selectMembers').modal('show');
});

function formatDate(date) {
  var monthNames = [
    "Jan", "Feb", "Mar",
    "Apr", "May", "Jun", "Jul",
    "Aug", "Sep", "Oct",
    "Nov", "Dec"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}


$(document).on("click",".ImgInstarLabel",function() {
  var txtClass = $(this).attr("for");
  if ($('#'+txtClass).is(':checked')) {
    $('.'+txtClass).removeClass('active');
  }else {
    $('.'+txtClass).addClass('active');
  }
});

$('.MemSelect').click(function(){
    $('form#selectMembersForm input:checkbox').each(function(){
        $(this).prop('checked',true);
   });
   $('.ImgInstar').addClass('active');
});

$('.MemClear').click(function(){
    $('form#selectMembersForm input:checkbox').each(function(){
        $(this).prop('checked',false);
   });
   $('.ImgInstar').removeClass('active');
});

$(window).resize(function () {
    $('.js-slider').not('.slick-initialized').slick('resize');
});

$(window).on('orientationchange', function () {
    $('.js-slider').not('.slick-initialized').slick('resize');
});

$('.slider-header').slick({
  infinite: true,
  arrows: true,
  slidesToShow: 6,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 5000,
  responsive: [
                {
                  breakpoint: 786,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 400,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }

              ]
});

$( "#selectMemHome" ).change(function() {
  var gen = $(this).val();
  var content = "";
  $.ajax({
  url: 'model/homeClassdb/homeAjax.php',
  type: 'POST',
  async: false,
  responseType: "json",
  data: {
    'gen':gen,
    'method':"selectMemHome"
  },
  success: function(res) {
    $('.divMemhome').remove();
    for (var i = 0; i < res.length; i++) {
      content += '<div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome">'+
            '<a href="?page=listMembers&memberId='+res[i]['mem_id']+'">'+
            '<div class="boxMemx">'+
              '<div class="ImgMem" style="background-image: url(data/Members/'+res[i]['mem_id']+'/s/'+res[i]['mem_images']+');"></div>'+
            '</div>'+
            '</a>'+
          '</div>';

    }
    $('.boxMem').html(content);

  },
  error: function(jqXHR, textStatus, errorThrown) {
    console.log(jqXHR, textStatus, errorThrown);
  }
  });
});

$('.searchMemIn').keyup(function(e){
    if(e.keyCode == 13){
        var Insname = $(this).val();
        var content = "";
        $.ajax({
        url: 'model/homeClassdb/homeAjax.php',
        type: 'POST',
        async: false,
        responseType: "json",
        data: {
          'Insname':Insname,
          'method':"searchMemIn"
        },
        success: function(res) {
          $('.divMemhome').remove();
          for (var i = 0; i < res.length; i++) {
            content += '<div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome">'+
                  '<a href="?page=listMembers&memberId='+res[i]['mem_id']+'">'+
                  '<div class="boxMemx">'+
                    '<div class="ImgMem" style="background-image: url(data/Members/'+res[i]['mem_id']+'/s/'+res[i]['mem_images']+');"></div>'+
                  '</div>'+
                  '</a>'+
                '</div>';

          }
          $('.boxMem').html(content);

        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        }
        });
    }
});

$('.LoadMoreIns').on('click',function(){
  var InsId = $("input[name='memIns[]']").map(function(){return $(this).val();}).get();
  var seMemIns = $("input[name='seMemIns']").val();

  $.ajax({
  url: 'model/homeClassdb/homeAjax.php',
  type: 'POST',
  async: false,
  responseType: "json",
  data: {
    'InsId':InsId,
    'seMemIns':seMemIns,
    'method':"LoadMoreIns"
  },
  success: function(res) {
    if(res.length < 8){
      $('.loadmore').hide();
    }
    for (var i = 0; i < res.length; i++) {
      $( ".homeInstagram" ).append('  <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 divinstar nopadding">'+
          '<input type="hidden" name="memIns[]" value="'+res[i]['meminsta_id']+'">'+
          '<a href="https://www.instagram.com/p/'+res[i]['meminsta_shortcode']+'" target="_blank">'+
          '<div class="boxSocialx">'+
            '<div class="ImgSocial" style="background-image: url('+res[i]['meminsta_link']+');"></div>'+
              '<div class="boxNameIns">'+res[i]['mem_nickname']+'.BNK48OFFICIAL</div>'+
          '</div>'+
          '</a>'+
        '</div>');
    }

  },
  error: function(jqXHR, textStatus, errorThrown) {
    console.log(jqXHR, textStatus, errorThrown);
  }
  });
});

$(document).on("click",".MemConf",function() {
  var searchIDs = $("#selectMembersForm input:checkbox:checked").map(function(){
     return $(this).val();
   }).get();

   $('input[name="seMemIns"]').val(searchIDs);
   $('#selectMembers').modal('hide');
   $('.loadmore').show();
    selectMemIns(searchIDs);

});

function selectMemIns(searchIDs){
  var content = "";
  $.ajax({
  url: 'model/homeClassdb/homeAjax.php',
  type: 'POST',
  async: false,
  responseType: "json",
  data: {
    'searchIDs':searchIDs,
    'method':"selectMemIns"
  },
  success: function(res) {

    content += '<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 divinstar nopadding">'+
      '<div class="boxSocialx">'+
        '<img class="ImgSocial" src="/plugins/images/group-3@2x.jpg" alt="">'+
          '<div class="selectMem"><img src="/plugins/images/group-17.png" alt=""></div>'+
      '</div>'+
    '</div>';

    for (var i = 0; i < res.length; i++) {
      content +='<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 divinstar nopadding">'+
          '<input type="hidden" name="memIns[]" value="'+res[i]['meminsta_id']+'">'+
          '<a href="https://www.instagram.com/p/'+res[i]['meminsta_shortcode']+'" target="_blank">'+
          '<div class="boxSocialx">'+
            '<div class="ImgSocial" style="background-image: url('+res[i]['meminsta_link']+');"></div>'+
              '<div class="boxNameIns">'+res[i]['mem_nickname']+'.BNK48OFFICIAL</div>'+
          '</div>'+
          '</a>'+
        '</div>';
    }
    $('.homeInstagram').html(content);
  },
  error: function(jqXHR, textStatus, errorThrown) {
    console.log(jqXHR, textStatus, errorThrown);
  }
  });
}


// $(document).on("click",".viewdetailschtxt",function() {
//     var txtClass = $(this).attr("role");
//
//     $.ajax({
//     url: 'model/homeClassdb/homeAjax.php',
//     type: 'POST',
//     async: false,
//     responseType: "json",
//     data: {
//       'calEventId':txtClass,
//       'method':"selectDateEvent"
//     },
//     success: function(rex) {
//       var conBox = "";
//       $('.classEven').html(rex[0]['schedule_name']);
//       $('.classEvendate').html(rex[0]['schedule_detail']);
//       var mem = rex[0]['schedule_oshimem'];
//
//       for (var i = 0; i < mem.length; i++) {
//         conBox += '<div class="boxschdate">'+mem[i]+'</div>';
//       }
//       $('.classboxschdate').html(conBox);
//       $('#viewdetailschtxt').modal('show');
//
//     },
//     error: function(jqXHR, textStatus, errorThrown) {
//       console.log(jqXHR, textStatus, errorThrown);
//     }
//     });
//
//
//
// });


$('.member-slide').slick({
  infinite: true,
  slidesToShow: 6,
  slidesToScroll: 6,
  autoplay: true,
  autoplaySpeed: 8000,
  responsive: [
                  {
                    breakpoint: 1200,
                    settings: {
                      slidesToShow: 5,
                      slidesToScroll: 5
                    }
                  },
                  {
                    breakpoint: 1024,
                    settings: {
                      slidesToShow: 4,
                      slidesToScroll: 4
                    }
                  },
                  {
                    breakpoint: 786,
                    settings: {
                      slidesToShow: 3,
                      slidesToScroll: 3
                    }
                  },
                  {
                    breakpoint: 600,
                    settings: {
                      slidesToShow: 2,
                      slidesToScroll: 2
                    }
                  },
                  {
                    breakpoint: 400,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1
                    }
                  }

                ]
});

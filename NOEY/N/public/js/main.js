$(window).on('load', function(){
  $('#loading').fadeOut('slow');
});

$(document).ready(function(){
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      $('#back_top').fadeIn();
    } else {
      $('#back_top').fadeOut();
    }
  });
 // scroll body to 0px on click
  $('#back_top').click(function () {
    $('body,html').animate({
      scrollTop: 0
    }, 500);
    return false;
  });
});

<!DOCTYPE html>
<html>
<head>

  <!-- Basic -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 

  <title>BNK48 SHOP</title>  

  <meta name="keywords" content="HTML5 Template" />
  <meta name="description" content="Porto - Responsive HTML5 Template">
  <meta name="author" content="okler.net">
  <link rel="icon" type="image/png" sizes="32x32" href="./img/favicon-32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="./img/favicon-16.png">

  <!-- Favicon -->

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <!--  <title>BNK48 The CAMPUS :: GET TICKETS</title>
    <link rel="icon" type="image/png" sizes="32x32" href="https://ticket.bnk48.com/img/Favicon-02.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://ticket.bnk48.com/img/Favicon-01.png"> -->
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="vendor/animate/animate.min.css">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/theme.css">
    <link rel="stylesheet" href="css/theme-elements.css">
    <link rel="stylesheet" href="css/theme-blog.css">
    <link rel="stylesheet" href="css/theme-shop.css">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

    <!-- Demo CSS -->


    <!-- Skin CSS -->
    <link rel="stylesheet" href="css/skins/skin-corporate-5.css"> 

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Head Libs -->
    <script src="vendor/modernizr/modernizr.min.js"></script>

  </head>
  <style type="text/css">
  body{
    background-image: url('{{asset('img/BG_BNK.jpg')}}');
  }
</style>
<body>

  <div class="body">
   <header id="header" class="header-narrow header-semi-transparent header-transparent-sticky-deactive header-transparent-bottom-border css-selector" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '1'}">
    <nav class="navbar navbar-light css-selecto" style="background-color: #c894c0;">
      <div class="header-container container">
        <div class="header-row">
          <div class="header-column">
            <div class="header-row">
              <div class="header-logo">
                <a href="\shop">
                  <img alt="Porto" width="140" height="52" src="img/logoshop.png">
                </a>
              </div>
            </div>
          </div>
          <div class="header-column justify-content-end">                  
            <div class="header-row">
              <div class="header-nav header-nav-dark-dropdown">
                <div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
                  <nav class="collapse">
                    <ul class="nav nav-pills" id="mainNav">
                     
                      <?php 
                      if(session()->has('id')){
                       
                        echo '<li class="dropdown dropdown-mega">';
                        echo '<a class="nav-bar" href="'.url('edit').'">สวัสดี '.session('name').'</a>';

                        echo '</li>';
                        echo '<li class="dropdown">';
                        echo '<a class="hoverli current" href="/logout"> ออกจากระบบ </a> ';                    
                        echo '</li>';
                        
                      }
                      else{
                       echo '<li class="dropdown">';
                       echo '<a class="hoverli current" href="/shopsingin"> เข้าสู่ระบบ </a> ';                    
                       echo '</li>';
                       echo '<li class="dropdown dropdown-mega">';
                       echo '<a class="nav-bar" href="/register">สมัครสมาชิก</a>';
                       echo '</li>';
                     }

                     ?>

                     

                   </ul>
                 </nav>
               </div>
               <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav css-selector">
                <i class="fas fa-bars"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<div align="center">
  <br><br><br><br>
  <img src="{{asset('img/slides/BN.png')}}"> 
  <br>

  <div class="hrhomebnk48shop css-selector">
    <div align="center">
      <br>
      <h3>สินค้า BNK48 </h3>,      
      <div class="row">
        <div class="col-lg-13">         
        </div> 
      </div>
      <div class="row nomargin">
        <div class="ImgOrder">
          <div class="col-sm-6 col-md-6 col-lg-4">
            <div class="boxOrder">
              <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-10">
                  
                  <br>
                  <br>
                  <img  src="{{asset('img/slides/BN1.png')}}" alt="">
                </div>
                <div class="NameOrder">
                  <span> BNK48 5th Single "BNK Festival" Music Card Edition</span>
                </div>
                <div class="row">
                  <div class="col-lg-4"></div>
                  <div class="col-lg-8">
                    <form action="/shopdetail">
                      <button type="submit" class="btn btn-outline-danger">View Details
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12">
              <div class="loadmorex">
              </div>
            </div>
            <br>
          </div>
        </div>

        
      </div>
    </div>
    <div class="container colboxso mt-5">
     <div class="row">
      <div class="row offset-lg-2">
        <div class="boxso col-lg-4">
          <a href="ticket" target="_blank">
            <img src="/img/slides/BN2.png" alt="" class="wh240_95">
            <br>
          </a>
        </div>
        <div class="boxso col-lg-4">
          <a href="schedule" target="_blank">
            <img src="/img/slides/BN3.png" alt="" class="wh240_95">
          </a>
        </div>
        
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
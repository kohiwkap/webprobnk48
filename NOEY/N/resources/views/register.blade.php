<!DOCTYPE html>
<html>
<head>

  <!-- Basic -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 

  <title>BNK48 CAMPUS GET TICKET</title>  

  <meta name="keywords" content="HTML5 Template" />
  <meta name="description" content="Porto - Responsive HTML5 Template">
  <meta name="author" content="okler.net">

  <!-- Favicon -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>BNK48 The CAMPUS :: GET TICKETS</title>
  <link rel="icon" type="image/png" sizes="32x32" href="https://ticket.bnk48.com/img/Favicon-02.png">
  <link rel="icon" type="image/png" sizes="16x16" href="https://ticket.bnk48.com/img/Favicon-01.png">
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

  <!-- Web Fonts  -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

  <!-- Vendor CSS -->
  <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
  <!-- Theme CSS -->
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <!-- Theme Custom CSS -->
  <link rel="stylesheet" href="css/custom.css">

</head>
<style type="text/css">
body{
  background-image: url('{{asset('img/BG_BNK.jpg')}}');
}
</style>
<body>

  <div class="body">
   <header id="header" class="header-narrow header-semi-transparent header-transparent-sticky-deactive header-transparent-bottom-border" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '1'}">
    <nav class="navbar navbar-light" style="background-color: #c894c0;">
      <div class="header-container container">
        <div class="header-row">
          <div class="header-column">
            <div class="header-row">
              <div class="header-logo">
                <a href="./ticket">
                  <img alt="img" width="155" height="52" src="img/Favicon-01.png">
                </a>
              </div>
            </div>
          </div>          
          <div class="header-column justify-content-end">                  
            <div class="header-row">
              <div class="header-nav header-nav-dark-dropdown">
                <div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
                  <nav class="collapse">
                    <ul class="nav nav-pills" id="mainNav">
                      <li class="dropdown">
                        <a class="hoverli current" href="/homes">
                          เข้าสู่ระบบ
                        </a>                     
                      </li>
                      <li class="dropdown dropdown-mega">
                        <a class="nav-bar" href="#member">
                          สมัครสมาชิก
                        </a>
                      </li>

                    </ul>
                  </nav>
                </div>
                <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                  <i class="fas fa-bars"></i>
                </button>

              </div>
            </div>
          </div>       
        </div>
      </div>
    </nav>
  </header>
</div>
</body>
</html>


<div class="modal-content">
  <div class="modal-header">

    <div class="row">

     
      <h4 class="">Register / สมัครสมาชิก</h4>
      
    </div>
  </div>

  <form action="{{url('register')}}" method="post" accept-charset="utf-8">
    {{csrf_field()}}
    <div class="modal-body bdsignup">
      <div class="row">
        <div class="col-lg-12 justify-content-center card-group ">
          <div class="col-md-5">
            <div class="formitem">
              <div class="txtsignin">
                <span>E-Mail</span>
              </div>
              <div class="">
                <input class="form-control boxitem" type="text" name="email" id="email" value="" onkeyup="this.value = isenChar(this.value,this)">
              </div>
            </div>

            <div class="formitem">
              <div class="txtsignin">
                <span>Password</span>
              </div>
              <div class="">
                <input class="form-control boxitem" type="password" name="passwd" id="passwd" value="" aria-autocomplete="list">
              </div>
            </div>

            <div class="formitem">
              <div class="txtsignin">
                <span>Confirm Password</span>
              </div>
              <div class="">
                <input class="form-control boxitem" type="password" name="passwdcf" value="">
              </div>
            </div>

            <div class="formitem">
              <div class="txtsignin">
                <span>Name / ชื่อ</span>
              </div>
              <div class="">
                <input class="form-control boxitem" type="text" name="firstname" id="firstname" value="">
              </div>
            </div>

            <div class="formitem">
              <div class="txtsignin">
                <span>Surname / นามสกุล</span>
              </div>
              <div class="">
                <input class="form-control boxitem" type="text" name="lastname" id="lastname" value="">
              </div>
            </div>

            <div class="formitem">
              <div class="txtsignin">
                <span>Mobile No.</span>
              </div>
              <div class="">
                <input class="form-control boxitem" type="number" name="telnumber" id="telnumber" value="">
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-12 justify-content-center">
               <button type="submit" class="btn btn-primary signinbtn text-center" onclick="signinbtn();">Sign up / ลงทะเบียน</button>
             </div>
           </div>
           
         </div>
       </div>

     </div>
   </div>
   <div class="modal-footer">
    <div class="row">
      <div class="col-md-12">

      </div>
    </div>
  </div>
</form>
</div>



</body>
</html>

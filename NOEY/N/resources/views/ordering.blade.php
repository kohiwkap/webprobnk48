<!DOCTYPE html>
<html>
<head>

  <!-- Basic -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 

  <title>BNK48 Unofficial Website</title>  

  <meta name="keywords" content="HTML5 Template" />
  <meta name="description" content="Porto - Responsive HTML5 Template">
  <meta name="author" content="okler.net">

  <!-- Favicon -->
  <link rel="icon" type="image/png" sizes="32x32" href="./img/favicon-32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="./img/favicon-16.png">
  <body background="BG_BNK">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="vendor/animate/animate.min.css">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/theme.css">
    <link rel="stylesheet" href="css/theme-elements.css">
    <link rel="stylesheet" href="css/theme-blog.css">
    <link rel="stylesheet" href="css/theme-shop.css">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">
    
    <!-- Demo CSS -->


    <!-- Skin CSS -->
    <link rel="stylesheet" href="css/skins/skin-corporate-8.css"> 

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Head Libs -->
    <script src="vendor/modernizr/modernizr.min.js"></script>

  </head>
  <body>

    <div class="body">
     <header id="header" class="header-narrow header-semi-transparent header-transparent-sticky-deactive header-transparent-bottom-border" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '1'}">
      <nav class="navbar navbar-light" style="background-color: #c894c0;">
        <div class="header-container container">
          <div class="header-row">
            <div class="header-column">
              <div class="header-row">
                <div class="header-logo">
                  <a href="./homes">
                    <img alt="Porto" width="150" height="52" src="img/bnk48logo2.png">
                  </a>
                </div>
              </div>
            </div>
            <div class="header-column justify-content-end">                  
              <div class="header-row">
                <div class="header-nav header-nav-dark-dropdown">
                  <div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
                  <nav class="collapse">
                      <ul class="nav nav-pills" id="mainNav">
                        <li class="dropdown dropdown-mega">
                          <a class="nav-bar" href="#">
                           ดูรายละเอียดการสั่งซื้อ
                          </a>
                        </li>
                        </ul>
                      <ul class="nav nav-pills" id="mainNav">
                        <li class="dropdown dropdown-mega">
                          <a class="nav-bar" href="#">
                            ลบ-เพิ่ม (สินค้า)
                          </a>
                        </li>
                        </ul>
                         <ul class="nav nav-pills" id="mainNav">
                        <li class="dropdown dropdown-mega">
                          <a class="nav-bar" href="#">
                           ดูรายการสินค้าของสมาชิก
                          </a>
                        </li>
                        </ul>
                      </nav>
                    </div>
  </body>
  </html>

<!DOCTYPE html>
<html>
<head>

  <!-- Basic -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 

  <title>BNK48 Unofficial Shopdetail</title>  

  <meta name="keywords" content="HTML5 Template" />
  <meta name="description" content="Porto - Responsive HTML5 Template">
  <meta name="author" content="okler.net">

  <!-- Favicon -->
  <link rel="icon" type="image/png" sizes="32x32" href="./img/favicon-32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="./img/favicon-16.png">
  <body background="BG_BNK">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="vendor/animate/animate.min.css">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/theme.css">
    <link rel="stylesheet" href="css/theme-elements.css">
    <link rel="stylesheet" href="css/theme-blog.css">
    <link rel="stylesheet" href="css/theme-shop.css">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">
    
    <!-- Demo CSS -->


    <!-- Skin CSS -->
    <link rel="stylesheet" href="css/skins/skin-corporate-8.css"> 

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Head Libs -->
    <script src="vendor/modernizr/modernizr.min.js"></script>

  </head>
  <body>

    <div class="body">
     <header id="header" class="header-narrow header-semi-transparent header-transparent-sticky-deactive header-transparent-bottom-border" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '1'}">
      <nav class="navbar navbar-light" style="background-color: #c894c0;">
        <div class="header-container container">
          <div class="header-row">
            <div class="header-column">
              <div class="header-row">
                <div class="header-logo">
                  <a href="./schedule">
                    <img alt="Porto" width="150" height="52" src="img/bnk48logo2.png">
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav><br>
      <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-9">
          <label class="">สินค้า BNK48 > BNK48 5th Single "BNK Festival" Music Card Edition</label>
          <div class="row">
            <div class="col-lg-4 ">
             <img alt="Porto" src="img/slides/BN1.png">
           </div>
           <div class="col-lg-8 ">
            <h4>BNK48 5th Single "BNK Festival" Music Card Edition</h4>
            <h2>ราคา 200 บาท</h2>
            <button type="button" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off">ซื้อตอนนี้
            </button>
            <button type="button" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off">เพิ่มใส่ตระกร้า
            </button>
          </div>
        </div>
        <div class="nav-link">
          <h5>[Pre-Order] BNK48 5th Single "BNK Festival"</h5>
          <h5>Music Card Edition ราคาใบละ 200 บาท พรีออเดอร์ได้ตั้งแต่วันที่ 8 พฤศจิกายน 2561 เวลา 12:00 น. ถึง 22 มกราคม 2562 เวลา 12:00 น. สิ่งที่จะได้รับ</h5>
          <h5>- บัตรแข็งที่ระลึก 1 ใบ จาก 51 แบบ</h5>
          <h5>- BNK48 5th Single BNK Festival Digital Tracks</h5>
          <h5>- ของขวัญพิเศษ สำหรับ BNK48 Official App </h5>
          <h5> - Code สำหรับลงคะแนน BNK48 6th Single Senbatsu General Election (Code จัดส่งทางอีเมลภายใน 24 ชั่วโมงหลังการชำระเงินเสร็จสมบูรณ์)</h5>
          <h5>*Digital Tracks และ Gift สามารถ Redeem ได้โดยขูดรหัสที่หลังบัตรแข็งที่ระลึก แล้วนำไป Redeem ได้ที่ BNK48 Official App</h5>
          <h5>*การสั่งซื้อแต่ละออเดอร์ จะจำกัดการซื้อ Music Card Edition อยู่ที่ 100 ใบต่อออเดอร์</h5>
          <h5>*เริ่มจัดส่งสินค้าตั้งแต่ 18 ธันวาคมเป็นต้นไป ตามลำดับออเดอร์</h5>
          <h5>*Music Card Edition จะมีการจำหน่ายที่งาน BNK48 1st Album RIVER 2-shot ในวันที่ 15-16 ธันวาคม และในกรณีที่ยังมีสินค้าคงเหลือก็จะนำไปจำหน่ายที่อีเวนท์ถัด ๆ ไปตามลำดับ โดยสำหรับ Music Card Edition ที่ซื้อจากงานอีเวนท์ จะได้รับ Code สำหรับลงคะแนน BNK48 6th Single Senbatsu General Election ในซองเลย</h5>
          <label>หมายเหตุ เงื่อนไขต่าง ๆ อาจจะมีการเปลี่ยนแปลงได้ตามความเหมาะสม</label>
        </div>
      </div>
    </div>
  </header>
</div>
</body>
</html>

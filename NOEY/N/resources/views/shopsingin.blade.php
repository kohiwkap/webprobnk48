<!DOCTYPE html>
<html>
<head>

  <!-- Basic -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 

  <title>BNK48 CAMPUS GET TICKET</title>  


  <!-- Favicon -->

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>BNK48 The CAMPUS :: GET TICKETS</title>
  <link rel="icon" type="image/png" sizes="32x32" href="https://ticket.bnk48.com/img/Favicon-02.png">
  <link rel="icon" type="image/png" sizes="16x16" href="https://ticket.bnk48.com/img/Favicon-01.png">
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

  <!-- Web Fonts  -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

  <!-- Vendor CSS -->
  <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="vendor/animate/animate.min.css">
  <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
  <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
  <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

  <!-- Theme CSS -->
  <link rel="stylesheet" href="css/theme.css">
  <link rel="stylesheet" href="css/theme-elements.css">
  <link rel="stylesheet" href="css/theme-blog.css">
  <link rel="stylesheet" href="css/theme-shop.css">

  <!-- Current Page CSS -->
  <link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
  <link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
  <link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

  <!-- Demo CSS -->


  <!-- Skin CSS -->
  <link rel="stylesheet" href="css/skins/skin-corporate-5.css"> 

  <!-- Theme Custom CSS -->
  <link rel="stylesheet" href="css/custom.css">

  <!-- Head Libs -->
  <script src="vendor/modernizr/modernizr.min.js"></script>

</head>
<style type="text/css">
body{
  background-image: url('{{asset('img/BG_BNK.jpg')}}');
}
</style>
<body>

  <div class="body">
   <header id="header" class="header-narrow header-semi-transparent header-transparent-sticky-deactive header-transparent-bottom-border" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '1'}">
    <nav class="navbar navbar-light" style="background-color: #c894c0;">
      <div class="header-container container">
        <div class="header-row">
          <div class="header-column">
            <div class="header-row">
              <div class="header-logo">
                <a href="./shop">
                  <img alt="Porto" width="145" height="52" src="img/logoshop.png">
                </a>
              </div>
            </div>
          </div>          
          <div class="header-column justify-content-end">                  
            <div class="header-row">
              <div class="header-nav header-nav-dark-dropdown">
                <div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
                  <nav class="collapse">
                    <ul class="nav nav-pills" id="mainNav">
                      <li class="dropdown">
                        <a class="hoverli current" href="/shopsingin">
                          เข้าสู่ระบบ
                        </a>                     
                      </li>
                      <li class="dropdown dropdown-mega">
                        <a class="nav-bar" href="/register">
                          สมัครสมาชิก
                        </a>
                      </li>

                    </ul>
                  </nav>
                </div>
                <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                  <i class="fas fa-bars"></i>
                </button>

              </div>
            </div>
          </div>       
        </div>
      </div>
    </nav>
  </header>
</div>
</body>
</html>
<br>
<br>
<br>
<br>
<br>

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    <div class="row">
      <div class="col-md-2 col-lg-2"></div>
      <div class="col-md-8 col-lg-8">
        <form id="signinForm" method="post" action="{{URL('shopsingin')}}">
          {{ csrf_field() }}
          <div class="txtlogin">
            <span class="stxtlogin tkbnktext_11">เข้าสู่ระบบ</span>
          </div>
          <div class="inpuslogin">

            <input type="hidden" name="mtfnc" value="signin">
            <div class="input-group input-group-signin">
             <span class="input-group-addon"><i class="fa fa-user"></i></span>
             <input type="text" class="form-control" id="firstname" name="email" placeholder="" value="" autocomplete="off">
           </div>
         </div>
         <div class="inpwdlogin last">
          <div class="input-group input-group-signin">
           <span class="input-group-addon"><i class="fa fa-key"></i></span>
           <input type="password" class="form-control" id="passwd" name="passwd" placeholder="" value="" autocomplete="off">
         </div>
       </div>



       <div class="btnsignlogin">
        <input type="submit" name="Login" value="Login" class="login-submit" />
        <!-- <button type="button" class="btn sbtnsignlogin tkbnktext_3" name="btnsignin">เข้าสู่ระบบ</button> -->
      </div>
      <div class="text-center font-sm _mt-40 or-text-outter">

        <div class="or-line"></div>
      </div>
      <div class="fb-wrapper fb-login">               
      </div>
    </form>

  </div>
  <div class="col-md-2 col-lg-2">
    <div class="borderRight"></div>
  </div>
</div>

</div>


<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
  <div class="row">
    <div class="col-md-2 col-lg-2"></div>
    <div class="col-md-8 col-lg-8">
      <div class="txtlogin">
        <span class="stxtRelogin tkbnktext_12">สมัครสมาชิก</span>
      </div>
      <div class="btnsignup">
        <!-- <a href="https://www.bnk48.com/#/user/register"><button type="button" class="btn sbtnsignup  tkbnktext_4" name="btnsignup">Sign Up</button></a> -->
        <a href="/register" class="btn btn-light">ลงทะเบียน</a>
      </div>
            <!-- <div class="txtRemark">
              <span class="stxtRemark">* Remark</span>
            </div> -->

          </div>
          <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
        </div>
        <div class="row rowdivlogin">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding">
            <!-- <div class="simagelogin"></div> -->
            <div class="card-image is-loaded" style="background-image: url(img/loginpicture2.jpg);" data-image-full="img/loginpicture.jpg">
             <img src="img/loginpicture.jpg" alt="">
           </div>
         </div>
       </div>
     </div>

   </div>
   <style>

   .my-navbar .container-menu-member{
    display: -webkit-box;
  }

  @media only screen and (max-width: 767px) {
    .my-navbar .container-menu-member{
      border-bottom: 1px solid #ccc;
    }
    .container-menu-member .container>.navbar-collapse.show{
      display: -webkit-block;
    }
    .my-navbar .container-menu-member .container>.navbar-collapse {
      padding: 0px;
    }
    .my-navbar .container-menu-member .navbar-nav {
      padding: 0px;
    }
    .my-navbar .container-menu-member .navbar-nav li{
      width: 100          border-bott%;
      om: 1px solid #ccc;
      padding: 0px;
    }
    .my-navbar .container-menu-member .navbar-nav li:last-of-type{
      border-bottom: none;
    }
    .my-navbar .container-menu-member .navbar-nav li a{
      text-align: left;
    }
  }

</style>



-----

<!-- Vendor -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
<script src="vendor/popper/umd/popper.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/common/common.min.js"></script>
<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="vendor/isotope/jquery.isotope.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="vendor/vide/vide.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Theme Custom -->
<script src="js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="js/theme.init.js"></script>

    <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-12345678-1', 'auto');
      ga('send', 'pageview');
    </script>
  -->

</body>
</html>

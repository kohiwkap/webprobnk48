<!DOCTYPE html>
<html>
<head>

  <!-- Basic -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 

  <title>BNK48 CAMPUS GET TICKET</title>  


  <meta name="keywords" content="HTML5 Template" />
  <meta name="description" content="Porto - Responsive HTML5 Template">
  <meta name="author" content="okler.net">

  <!-- Favicon -->

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>BNK48 The CAMPUS :: GET TICKETS</title>
  <link rel="icon" type="image/png" sizes="32x32" href="https://ticket.bnk48.com/img/Favicon-02.png">
  <link rel="icon" type="image/png" sizes="16x16" href="https://ticket.bnk48.com/img/Favicon-01.png">
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

  <!-- Web Fonts  -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

  <!-- Vendor CSS -->
  <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="vendor/animate/animate.min.css">
  <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
  <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
  <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

  <!-- Theme CSS -->
  <link rel="stylesheet" href="css/theme.css">
  <link rel="stylesheet" href="css/theme-elements.css">
  <link rel="stylesheet" href="css/theme-blog.css">
  <link rel="stylesheet" href="css/theme-shop.css">

  <!-- Current Page CSS -->
  <link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
  <link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
  <link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

  <!-- Demo CSS -->


  <!-- Skin CSS -->
  <link rel="stylesheet" href="css/skins/skin-corporate-5.css"> 

  <!-- Theme Custom CSS -->
  <link rel="stylesheet" href="css/custom.css">

  <!-- Head Libs -->
  <script src="vendor/modernizr/modernizr.min.js"></script>

</head>
<style type="text/css">
body{
  background-image: url('{{asset('img/BG_BNK.jpg')}}');
}
</style>
<body>

  <div class="body">
   <header id="header" class="header-narrow header-semi-transparent header-transparent-sticky-deactive header-transparent-bottom-border" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '1'}">
    <nav class="navbar navbar-light" style="background-color: #c894c0;">
      <div class="header-container container css-selector">
        <div class="header-row">
          <div class="header-column">
            <div class="header-row">
              <div class="header-logo">
                <a href="./ticket">
                  <img alt="BNK Ticket  " width="155" height="52" src="img/logoTicketBNK48.png">
                </a>
              </div>
            </div>
          </div>          
          <div class="header-column justify-content-end">                  
            <div class="header-row">
              <div class="header-nav header-nav-dark-dropdown">
                <div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
                  <nav class="collapse">
                    <ul class="nav nav-pills" id="mainNav">
                      <?php 
                      if(session()->has('id')){

                        echo '<li class="dropdown dropdown-mega">';
                        echo '<a class="nav-bar" href="">สวัสดี '.session('name').'</a>';
                        echo '</li>';
                        echo '<li class="dropdown">';
                        echo '<a class="hoverli current" href="/logout"> ออกจากระบบ </a> ';                    
                        echo '</li>';

                      }
                      else{
                       echo '<li class="dropdown">';
                       echo '<a class="hoverli current" href="\singin"> เข้าสู่ระบบ </a> ';                    
                       echo '</li>';
                       echo '<li class="dropdown dropdown-mega">';
                       echo '<a class="nav-bar" href="/register">สมัครสมาชิก</a>';
                       echo '</li>';
                     }

                     ?>

                   </ul>
                 </nav>
               </div>


             </div>
           </div>
         </div>       
       </div>
     </div>
   </nav>
 </header>


</div>
</body>
</html>
<br>
<br>
<br>
<br>
<div class="modal-content ">
  <div class="modal-header hrsignup ">
    <div class="row ">
      <form method="post" action="/update" >       
       {{ csrf_field() }}
      <div class="col-md-12 css-selector">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
        <h4 class="modal-title">แก้ไขข้อมูลส่วนตัว</h4> 
         </div>
        <input type="text" name="id" value="<?= session('id') ?>">
        <input type="text" name="email" value="<?= session('email') ?>">
        <input type="text" name="passwd" value="<?= session('passwd') ?>">
        <input type="text" name="firstname" value="<?= session('firstname') ?>">
        <input type="text" name="lastname" value="<?= session('lastname') ?>">
        <input type="text" name="telnumber" value="<?= session('telnumber') ?>">

        <button type="submit" class="btn btn-primary signinbtn text-center" onclick="signinbtn();">แก้ไขข้มูล</button>
  
  </form>
  </div>

  
</div>


<!-- Vendor -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
<script src="vendor/popper/umd/popper.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/common/common.min.js"></script>
<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="vendor/isotope/jquery.isotope.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="vendor/vide/vide.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Theme Custom -->
<script src="js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="js/theme.init.js"></script>

    <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-12345678-1', 'auto');
      ga('send', 'pageview');
    </script>
  -->

</body>
</html>

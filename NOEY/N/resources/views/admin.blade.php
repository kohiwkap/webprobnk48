<!DOCTYPE html>
<html>
<head>
	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> 

	<title>BNK48 Unofficial Admin</title>  

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<!-- Favicon -->
	<link rel="icon" type="image/png" sizes="32x32" href="./img/favicon-32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="./img/favicon-16.png">
	<body background="BG_BNK">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
		<link rel="stylesheet" href="vendor/animate/animate.min.css">
		<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-elements.css">
		<link rel="stylesheet" href="css/theme-blog.css">
		<link rel="stylesheet" href="css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

		<!-- Demo CSS -->


		<!-- Skin CSS -->
		<link rel="stylesheet" href="css/skins/skin-corporate-8.css"> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">

		<!-- Head Libs -->
		<script src="vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body>
		<div class="body">
			<header id="header" class="header-narrow header-semi-transparent header-transparent-sticky-deactive header-transparent-bottom-border" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '1'}">
				<nav class="navbar navbar-light" style="background-color: #c894c0;">
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row row">
									<div class="header-logo col-lg-10">
										<a href="./schedule">
											<img alt="Porto" width="150" height="52" src="img/bnk48logo2.png">
											<!-- <h1>Admin</h1> -->
										</a>
									</div>
									<div class="col-lg-2">
										<div class="mt-3">
											<h1>Admin</h1>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</nav>
			</header>
			<br>
			<br>
			<br>
			<br>
			<div role="main" class="main">
				<section class="section section-no-background section-no-border m-0">
					<div class="container">
						<div class="row mb-4">
							<div class="col-lg-3">
								<div class="tabs tabs-vertical tabs-left tabs-navigation">
									<ul class="nav nav-tabs col-sm-3">
										<li class="nav-item active">
											<a class="nav-link" href="/Editdata" data-toggle="tab"> จัดการข้อมูล Member</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="/Newsupdate" data-toggle="tab"> จัดการข้อมูลข่าวสาร</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="/ordering" data-toggle="tab"> จัดการสินค้า</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>

		</div>
	</body>
	</html>
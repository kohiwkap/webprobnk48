<!DOCTYPE html>
<html>
<head>

  <!-- Basic -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 

  <title>BNK48 Unofficial Website</title>  

  <meta name="keywords" content="HTML5 Template" />
  <meta name="description" content="Porto - Responsive HTML5 Template">
  <meta name="author" content="okler.net">
  <link rel="icon" type="image/png" sizes="32x32" href="./img/favicon-32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="./img/favicon-16.png">

  <!-- Favicon -->
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>BNK48 The CAMPUS :: GET TICKETS</title>
  <link rel="icon" type="image/png" sizes="32x32" href="https://ticket.bnk48.com/img/Favicon-02.png">
  <link rel="icon" type="image/png" sizes="16x16" href="https://ticket.bnk48.com/img/Favicon-01.png">
  <body background="BG_BNK">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    <!-- Web Fonts  -->
<link href="https://fonts.googleapis.com/css?family=Kanit:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=thai" rel="stylesheet"  type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="vendor/animate/animate.min.css">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/theme.css">
    <link rel="stylesheet" href="css/theme-elements.css">
    <link rel="stylesheet" href="css/theme-blog.css">
    <link rel="stylesheet" href="css/theme-shop.css">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">
    
    <!-- Demo CSS -->


    <!-- Skin CSS -->
    <link rel="stylesheet" href="css/skins/skin-corporate-8.css"> 

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Head Libs -->
    <script src="vendor/modernizr/modernizr.min.js"></script>

  </head>
  <style type="text/css">
  body{
    background-image: url('{{asset('img/BG_BNK.jpg')}}');
  }
</style>
<body>

  <div class="body">
   <header id="header" class="header-narrow header-semi-transparent header-transparent-sticky-deactive header-transparent-bottom-border css-selector" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '1'}">
    <nav class="navbar navbar-light" style="background-color: #c894c0;">
      <div class="header-container container">
        <div class="header-row">
          <div class="header-column">
            <div class="header-row">
              <div class="header-logo">
                <a href="./homes">
                  <img alt="bnk48" width="150" height="52" src="img/bnk48logo2.png">
                </a>
              </div>
            </div>
          </div>
          <div class="header-column justify-content-end">                  
            <div class="header-row">
              <div class="header-nav header-nav-dark-dropdown">
                <div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
                  <nav class="collapse">
                    <ul class="nav nav-pills" id="mainNav">
                      <li class="dropdown">
                        <a class="hoverli current" href="\homes">
                          Home
                        </a>
                        <ul class="">                             
                        </ul>
                      </li>
                      <li class="dropdown dropdown-mega">
                        <a class="nav-bar" href="\admin">
                          admin
                        </a>

                      </li>                    
                      
                      <li class="dropdown">
                        <a class="nav-bar" href="\ticket">
                          Ticket
                        </a>                              
                        <li class="dropdown">
                          <a class="nav-bar" href="\shop">
                            Shop
                          </a>                              
                        </li>
                      </ul>
                    </nav>
                  </div>
                  <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                    <i class="fas fa-bars"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

    <div role="main" class="main">
      <div class="slider-container rev_slider_wrapper css-selector">
        <div id="revolutionSlider" class="slider rev_slider" data-version="5.4.7" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 500, 'responsiveLevels': [4096,1200,992,500]}">
          <ul>
            <li data-transition="fade">

              <img src="img/slides/slide-corporate-5.jpg"  
              alt=""
              data-bgposition="center center" 
              data-bgfit="cover" 
              data-bgrepeat="no-repeat" 
              class="rev-slidebg">
              <!-- ตัวหนังสือ -->                                 
            </li>
            <li data-transition="fade">

              <img src="img/slides/slide-corporate-6.jpg"  
              alt=""
              data-bgposition="center center" 
              data-bgfit="cover" 
              data-bgrepeat="no-repeat" 
              class="rev-slidebg">             
            </li>
          </ul>
        </div>
      </div>

      <div class="container">
        <div class="row mb-">
          <div class="col-lg-15 css-selector">         
            <div class="row Newsx nomargin">  
              <br>

              <h3 class="col-lg-12 text-center" style="color: #CC00FF " >
               <br>     
               <strong>News  </strong>
               <!-- <nav class="navbar navbar-light bg-dark"> -->
               </h3>
               <h5 class ="col-lg-12 text-right"> BNK48 UPDATE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <!-- <form class="col-lg-0text-left"> -->  
                   <button type="button" class="btn btn-outline-danger">See more</button>                  
                 </form>
               </h5>
               </nav   




               <div class="row">
                <div class="col-lg-4">
                  <div class="feature-box feature-box-secondary feature-box-style-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
                    <div class="feature-box-icon">
                     <img  width="45" height="54" src="img/digitallive.jpg"> 
                     <br> <a class="hoverli current" href="\homes" >
                      <h4 class="mb-2"> &nbsp; Digital Live Studio</h4>
                    </a> 
                    <div  style="color: #FF0000" >&nbsp;New!</div>                                 
                  </div>
                  <div class="feature-box-info">
                    <br>
                    <p class="mb-4">ประกาศรายชื่อสมาชิกที่เข้าร่วม
                      Digital Live Studio
                      ประจำวันที่ 13 ธันวาคม 2561
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 css-selecto">
                <div class="feature-box feature-box-secondary feature-box-style-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
                  <div class="feature-box-icon">
                   <img  width="45" height="54" src="img/digitallive.jpg"> 
                   <br> <a class="hoverli current" href="\homes" >
                    <h4 class="mb-2"> &nbsp; Digital Live Studio  </h4>  
                  </a>    
                  <div  style="color: #FF0000" >&nbsp;New!</div>         
                </div>
                <div class="feature-box-info">
                  <br>
                  <p class="mb-4">ประกาศรายชื่อสมาชิกที่เข้าร่วม
                    Digital Live Studio
                    ประจำวันที่ 13 ธันวาคม 2561
                  </p>
                </div>
              </div>
            </div><div class="col-lg-4">
              <div class="feature-box feature-box-secondary feature-box-style-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
                <div class="feature-box-icon">
                 <img  width="45" height="54" src="img/digitallive.jpg"> 
                 <br> <a class="hoverli current" href="\homes" >
                  <h4 class="mb-2"> &nbsp; Digital Live Studio  </h4>  
                </a>   
                <div  style="color: #FF0000" >&nbsp;New!</div>          
              </div>
              <div class="feature-box-info">
                <br>
                <p class="mb-4">ประกาศรายชื่อสมาชิกที่เข้าร่วม
                  Digital Live Studio
                  ประจำวันที่ 13 ธันวาคม 2561
                </p>
              </div>
            </div>
          </div><div class="col-lg-4">
            <div class="feature-box feature-box-secondary feature-box-style-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
              <div class="feature-box-icon">
               <img  width="45" height="54" src="img/digitallive.jpg"> 
               <br> <a class="hoverli current" href="\homes" >
                <h4 class="mb-2"> &nbsp; Digital Live Studio  </h4>  
              </a>             
            </div>
            <div class="feature-box-info">
              <br>
              <p class="mb-4">ประกาศรายชื่อสมาชิกที่เข้าร่วม
                Digital Live Studio
                ประจำวันที่ 13 ธันวาคม 2561
              </p>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="feature-box feature-box-secondary feature-box-style-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
            <div class="feature-box-icon">
             <img  width="45" height="54" src="img/digitallive.jpg"> 
             <br> <a class="hoverli current" href="\homes" >
              <h4 class="mb-2"> &nbsp; Digital Live Studio  </h4>  
            </a>             
          </div>

        </div>
      </div>
      <div class="col-lg-4">
        <div class="feature-box feature-box-secondary feature-box-style-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
          <div class="feature-box-icon">
           <img  width="45" height="54" src="img/digitallive.jpg"> 
           <br> <a class="hoverli current" href="\homes" >
            <h4 class="mb-2"> &nbsp; Digital Live Studio  </h4>  
          </a>             
        </div>
        <div class="feature-box-info">
          <br>
          <p class="mb-4">ประกาศรายชื่อสมาชิกที่เข้าร่วม
            Digital Live Studio
            ประจำวันที่ 13 ธันวาคม 2561
          </p>
        </div>
      </div>
    </div>

  </div>
</section>
 <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
       <a class="hoverli current"   style="color: #CC00FF "> 
        <h2 class="heading-dark mt-5"><strong>SCHEDULE</strong>  </a> </h2>
      </div>            
      <h5 class ="col-lg-12 text-right">           
       <form class="col-lg-0text-left">                   
         <button type="button" class="btn btn-outline-danger">See more</button>               
       </form>
     </h5>
     <div class="row">              
      <div class="col-lg-4">
        <div class="col-lg-10 text-center">
          <div class="recent-posts mt-5">
            <article class="post">
              <div class="date">
                <span class="day">22</span>
                <span class="month background-color-secondary">DEC</span>
              </div>
              <h4><a href="blog-post.html">BNK48 1st Album RIVER : 2Shot Event</a></h4>
              <p><br> Place : 4th FLOOR CentralPlaza WestGate <br>
                Member : (1)<br>
              Cherprang</p>                  
            </div>                      
          </article>
        </div>
        <a href="#" class="btn btn-primary mt-3 mb-2">Read More</a>
      </div>
      <div class="col-lg-4">
        <div class="recent-posts mt-4">
          <div class="col-lg-10 text-center">
            <article class="post">
              <div class="date">
                <span class="day">23</span>
                <span class="month background-color-secondary">DEC</span>
              </div>
              <h4><a href="blog-post.html">EVENT : The Face of Ability</a></h4>
              <p>Date&Time : 23 December (10.00-11.30)<br>
                Place : ลานเอนกประสงค์ Amphitheater สยามสแควร์                      
              </p>
            </article>
          </div>
          <a href="#" class="btn btn-primary mt-3 mb-2">Read More</a>
        </article>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="recent-posts mt-4">
        <div class="col-lg-10 text-center">
          <article class="post">
            <div class="date">
              <span class="day">25</span>
              <span class="month background-color-secondary">DEC</span>
            </div>
            <h4><a href="blog-post.html">EVENT : Smiley Celebration</a></h4>
            <p><br>Date&Time : 25 Dec 18<br>
              Place : Central บางนา<br>
            Member : (6)</p>
          </article>
        </div>

        <a href="#" class="btn btn-primary mt-3 mb-2">Read More</a>
      </article>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="recent-posts mt-4">
      <div class="col-lg-10 text-center">
        <article class="post">
          <div class="date">
            <span class="day">19</span>
            <span class="month background-color-secondary">Jan</span>
          </div>
          <h4><a href="blog-post.html">Trang music festival</a></h4><br>
          <p>Date&Time : 19 Jan. 2019<br>
            Place : Trang<br>
          Member : ( 16 )</p>
        </article>
      </div>
      <a href="#" class="btn btn-primary mt-3 mb-2">Read More</a>
    </article>
  </div>
</div>
<div class="col-lg-4">
  <div class="recent-posts mt-4">
    <div class="col-lg-10 text-center">
      <article class="post">
        <div class="date">
          <span class="day">26</span>
          <span class="month background-color-secondary">Jan</span>
        </div>
        <h4><a href="blog-post.html">ประกาศผล <br>
        BNK48 6th Single Senbatsu General Election</a></h4>
        <p>Place : Impact Arena Muang Thong Thani</p>   
      </article>
    </div>                  
    <a href="#" class="btn btn-primary mt-3 mb-2">Read More</a>
  </article>
</div>
</div>
<div class="col-lg-4">
  <div class="col-lg-10 text-center">
    <div class="recent-posts mt-4">
      <article class="post">
        <div class="date">
          <span class="day">10</span>
          <span class="month background-color-secondary">FEB</span>
        </div>
        <h4><a href="blog-post.html">Event : วิ่งข้ามสมุทร 2019</a></h4>
        <p>จัดขึ้นเนื่องในโอกาสครบรอบ 99 ปี พลเอกเปรม ติณสูลานนท์<br>
          Place : สงขลา<br>
        Member : (16)</p>
      </article>
    </div>
  </div>
  <a href="#" class="btn btn-primary mt-3 mb-2">Read More</a>
</article>
</div>
</div>

</div>
</div>
</section>
</div>
</div>  

<section class="section section-background section-center mt-5 mb-5" style="background-image: url(img/BG_BNK.jpg);">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-10 css-selector">
        <h2 class="heading-dark mt-5"><strong>MEMBER</strong></h2> 
        <div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options="{'items': 1, 'loop': false}">
          <div>
            <div class="col">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 divMemMembers">
                <div class="boxMemx" role="52">
                  <!-- <img  width="45" height="54" src="img/digitallive.jpg">  -->
                  <div class="">
                    <div class="row">
                      <div class="col-sm-3">
                        <div >

                          <img src="{{asset('img/member/aom.png')}}"> 
                          <div class="nameMem">
                           AOM             
                         </div>
                         <div class="positionMem">
                         </div>
                         <div class="briMem">
                          20 September 1995          
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div >

                        <img src="{{asset('img/member/bamboo.png')}}"> 
                        <div class="nameMem">
                          BAMBOO              
                        </div>
                        <div class="positionMem">
                        </div>
                        <div class="briMem">
                          3 September 2002          
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div >

                        <img src="{{asset('img/member/cake.png')}}"> 
                        <div class="nameMem">
                          CAKE              
                        </div>
                        <div class="positionMem">
                        </div>
                        <div class="briMem">
                          18 November 1996        
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div >

                        <img src="{{asset('img/member/Cherprang.png')}}"> 
                        <div class="nameMem">
                          CHERPRANG             
                        </div>
                        <div class="positionMem">
                        </div>
                        <div class="briMem">
                          2 May 1996          
                        </div>
                      </div>
                    </div><div class="col-sm-3">
                      <div >

                        <img src="{{asset('img/member/deenee.png')}}"> 
                        <div class="nameMem">
                          DEENEE              
                        </div>
                        <div class="positionMem">
                        </div>
                        <div class="briMem">
                          28 November 2001        
                        </div>
                      </div>
                    </div><div class="col-sm-3">
                      <div >

                        <img src="{{asset('img/member/faii.png')}}"> 
                        <div class="nameMem">
                          FAII             
                        </div>
                        <div class="positionMem">
                        </div>
                        <div class="briMem">
                          28 June 1996        
                        </div>
                      </div>
                    </div><div class="col-sm-3">
                      <div >

                        <img src="{{asset('img/member/fifa.png')}}"> 
                        <div class="nameMem">
                          FIFA              
                        </div>
                        <div class="positionMem">
                        </div>
                        <div class="briMem">
                          6 November 2001           
                        </div>
                      </div>
                    </div><div class="col-sm-3">
                      <div >

                        <img src="{{asset('img/member/fond.png')}}"> 
                        <div class="nameMem">
                         FOND             
                       </div>
                       <div class="positionMem">
                       </div>
                       <div class="briMem">
                         3 December 2002            
                       </div>
                     </div>
                   </div><div class="col-sm-3">
                    <div >

                      <img src="{{asset('img/member/gygee.png')}}"> 
                      <div class="nameMem">
                       GYGEE             
                     </div>
                     <div class="positionMem">
                     </div>
                     <div class="briMem">
                      4 October 2001           
                    </div>
                  </div>
                </div><div class="col-sm-3">
                  <div >

                   <img src="{{asset('img/member/rina.png')}}"> 
                   <div class="nameMem">
                     IZURINA         
                   </div>
                   <div class="positionMem">
                   </div>
                   <div class="briMem">
                    26 November 1995       
                  </div>
                </div>
              </div><div class="col-sm-3">
                <div >

                  <img src="{{asset('img/member/jaa.png')}}"> 
                  <div class="nameMem">
                   JAA             
                 </div>
                 <div class="positionMem">
                 </div>
                 <div class="briMem">
                  20 January 2003           
                </div>
              </div>
            </div><div class="col-sm-3">
              <div >
                <img src="{{asset('img/member/jane.png')}}"> 
                <div class="nameMem">
                  JANE           
                </div>
                <div class="positionMem">
                </div>
                <div class="briMem">
                 23 March 2000          
               </div>
             </div>
           </div>
         </div>
       </div>

       <!-- <img   src="img/member/aom.png">  -->
       <!-- <div style="background-image: url(members/aom.png);"></div> -->
                  <!-- <div class="boxnameMem">
                    <div class="nameMem">
                      AOM              
                    </div>
                    <div class="positionMem">
                    </div>
                    <div class="briMem">
                      20 September 1995          
                    </div>


                    <img   src="img/member/aom.png"> 
                    <div style="background-image: url(members/aom.png);"></div>
                    <div class="boxnameMem">
                      <div class="nameMem">
                      AOM              </div>
                      <div class="positionMem">
                      </div>
                      <div class="briMem">
                        20 September 1995          
                      </div> -->
                    </div>
                  </div>
                </div>    
              </div>
              <blockquote>                 
                <div class="boxMemx" role="52">
                  <!-- <img  width="45" height="54" src="img/digitallive.jpg">  -->
                  <div class="">
                    <div class="row">
                      <div class="col-sm-3">
                        <div >
                          <img src="{{asset('img/member/jennis.png')}}"> 
                          <div class="nameMem">
                            JENNIS            
                          </div>
                          <div class="positionMem">
                          </div>
                          <div class="briMem">
                            4 July 2000        
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div >
                          <img src="{{asset('img/member/jib.png')}}"> 
                          <div class="nameMem">
                            JIB             
                          </div>
                          <div class="positionMem">
                          </div>
                          <div class="briMem">
                            4 July 2002         
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div >
                          <img src="{{asset('img/member/june.png')}}"> 
                          <div class="nameMem">
                            JUNE              
                          </div>
                          <div class="positionMem">
                          </div>
                          <div class="briMem">
                            4 July 2000           
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div >
                          <img src="{{asset('img/member/kaew.png')}}"> 
                          <div class="nameMem">
                            KAEW           
                          </div>
                          <div class="positionMem">
                          </div>
                          <div class="briMem">
                            31 March  1994         
                          </div>
                        </div>
                      </div><div class="col-sm-3">
                        <div >
                          <img src="{{asset('img/member/kaimook.png')}}"> 
                          <div class="nameMem">
                            KAIMOOK              
                          </div>
                          <div class="positionMem">
                          </div>
                          <div class="briMem">
                            27 August  1997         
                          </div>
                        </div>
                      </div><div class="col-sm-3">
                        <div >
                          <img src="{{asset('img/member/kate.png')}}"> 
                          <div class="nameMem">
                            KATE            
                          </div>
                          <div class="positionMem">
                          </div>
                          <div class="briMem">
                            9 July 2001         
                          </div>
                        </div>
                      </div><div class="col-sm-3">
                        <div >
                          <img src="{{asset('img/member/khamin.png')}}"> 
                          <div class="nameMem">
                            KHAMIN              
                          </div>
                          <div class="positionMem">
                          </div>
                          <div class="briMem">
                            23 April  1999       
                          </div>
                        </div>
                      </div><div class="col-sm-3">
                        <div >
                         <img src="{{asset('img/member/kheng.png')}}"> 
                         <div class="nameMem">
                          KHENG              
                        </div>
                        <div class="positionMem">
                        </div>
                        <div class="briMem">
                         26 March  2000       
                       </div>
                     </div>
                   </div><div class="col-sm-3">
                    <div >
                      <img src="{{asset('img/member/korn.png')}}"> 
                      <div class="nameMem">
                        KORN             
                      </div>
                      <div class="positionMem">
                      </div>
                      <div class="briMem">
                       21 January  1999      
                     </div>
                   </div>
                 </div><div class="col-sm-3">
                  <div >
                   <img src="{{asset('img/member/maira.png')}}"> 
                   <div class="nameMem">
                    MAIRA      
                  </div>
                  <div class="positionMem">
                  </div>
                  <div class="briMem">
                   24 February  1997      
                 </div>
               </div>
             </div><div class="col-sm-3">
              <div >
                <img src="{{asset('img/member/mewnich.png')}}"> 
                <div class="nameMem">
                  MEWNICH    
                </div>
                <div class="positionMem">
                </div>
                <div class="briMem">
                 11 March  2002     
               </div>
             </div>
           </div><div class="col-sm-3">
            <div >
              <img src="{{asset('img/member/mind.png')}}"> 
              <div class="nameMem">
                MIND            
              </div>
              <div class="positionMem">
              </div>
              <div class="briMem">
                6 September 2001          
              </div>
            </div>
          </div>
        </div>
      </blockquote>
      <blockquote>                 
        <div class="boxMemx" role="52">
          <!-- <img  width="45" height="54" src="img/digitallive.jpg">  -->
          <div class="">
            <div class="row">
              <div class="col-sm-3">
                <div >
                 <img src="{{asset('img/member/minmin.png')}}"> 
                 <div class="nameMem">
                  MINMIN              
                </div>
                <div class="positionMem">
                </div>
                <div class="briMem">
                  20 March 1997        
                </div>
              </div>
            </div>
            <div class="col-sm-3">
              <div >
                <img src="{{asset('img/member/miori.png')}}"> 
                <div class="nameMem">
                  MIORI              
                </div>
                <div class="positionMem">
                </div>
                <div class="briMem">
                  30 September 1998          
                </div>
              </div>
            </div>
            <div class="col-sm-3">
              <div >
                <img src="{{asset('img/member/mobile.png')}}"> 
                <div class="nameMem">
                  MOBILE              
                </div>
                <div class="positionMem">
                </div>
                <div class="briMem">
                  9 July 2002          
                </div>
              </div>
            </div>
            <div class="col-sm-3">
              <div >
                <img src="{{asset('img/member/music.png')}}"> 
                <div class="nameMem">
                  MUSIC              
                </div>
                <div class="positionMem">
                </div>
                <div class="briMem">
                  24 February 2001          
                </div>
              </div>
            </div><div class="col-sm-3">
              <div >
                <img src="{{asset('img/member/myyu.png')}}"> 
                <div class="nameMem">
                  MYYU              
                </div>
                <div class="positionMem">
                </div>
                <div class="briMem">
                  20 October 1999          
                </div>
              </div>
            </div><div class="col-sm-3">
              <div >
                <img src="{{asset('img/member/namneung.png')}}"> 
                <div class="nameMem">
                  NAMNEUNG              
                </div>
                <div class="positionMem">
                </div>
                <div class="briMem">
                  11 November 1996          
                </div>
              </div>
            </div><div class="col-sm-3">
              <div >
                <img src="{{asset('img/member/namsai.png')}}"> 
                <div class="nameMem">
                  NAMSAI              
                </div>
                <div class="positionMem">
                </div>
                <div class="briMem">
                  26 October 1999          
                </div>
              </div>
            </div><div class="col-sm-3">
              <div >
                <img src="{{asset('img/member/nat.png')}}"> 
                <div class="nameMem">
                  NATHERINE              
                </div>
                <div class="positionMem">
                </div>
                <div class="briMem">
                  11 November 1999          
                </div>
              </div>
            </div><div class="col-sm-3">
              <div >
                <img src="{{asset('img/member/new.png')}}"> 
                <div class="nameMem">
                  NEW              
                </div>
                <div class="positionMem">
                </div>
                <div class="briMem">
                  2 January 2003          
                </div>
              </div>
            </div><div class="col-sm-3">
              <div >
                <img src="{{asset('img/member/niki.png')}}"> 
                <div class="nameMem">
                  NIKI              
                </div>
                <div class="positionMem">
                </div>
                <div class="briMem">
                  26 January 2005          
                </div>
              </div>
            </div><div class="col-sm-3">
              <div >
                <img src="{{asset('img/member/nine.png')}}"> 
                <div class="nameMem">
                  NINE              
                </div>
                <div class="positionMem">
                </div>
                <div class="briMem">
                  11 November 2000          
                </div>
              </div>
            </div><div class="col-sm-3">
              <div >
                <img src="{{asset('img/member/nink.png')}}"> 
                <div class="nameMem">
                  NINK              
                </div>
                <div class="positionMem">
                </div>
                <div class="briMem">
                  3 February 2000  
                </div>
              </div>
            </div>
          </div>
        </blockquote>
        <blockquote>                 
          <div class="boxMemx" role="52">
            <!-- <img  width="45" height="54" src="img/digitallive.jpg">  -->
            <div class="">
              <div class="row">
                <div class="col-sm-3">
                  <div >
                    <img src="{{asset('img/member/noey.png')}}"> 
                    <div class="nameMem">
                      NOEY              
                    </div>
                    <div class="positionMem">
                    </div>
                    <div class="briMem">
                      9 April 1997        
                    </div>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div >
                    <img src="{{asset('img/member/oom.png')}}"> 
                    <div class="nameMem">
                      OOM              
                    </div>
                    <div class="positionMem">
                    </div>
                    <div class="briMem">
                      29 September 2002          
                    </div>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div >
                    <img src="{{asset('img/member/orn.png')}}"> 
                    <div class="nameMem">
                      ORN              
                    </div>
                    <div class="positionMem">
                    </div>
                    <div class="briMem">
                      3 February 1997         
                    </div>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div >
                    <img src="{{asset('img/member/pakwan.png')}}"> 
                    <div class="nameMem">
                      PAKWAN              
                    </div>
                    <div class="positionMem">
                    </div>
                    <div class="briMem">
                      18 February 2000          
                    </div>
                  </div>
                </div><div class="col-sm-3">
                  <div >
                    <img src="{{asset('img/member/panda.png')}}"> 
                    <div class="nameMem">
                      PANDA              
                    </div>
                    <div class="positionMem">
                    </div>
                    <div class="briMem">
                      10 October 1997          
                    </div>
                  </div>
                </div><div class="col-sm-3">
                  <div >
                    <img src="{{asset('img/member/phukkohm.png')}}"> 
                    <div class="nameMem">
                      PHUKKOHM              
                    </div>
                    <div class="positionMem">
                    </div>
                    <div class="briMem">
                      28 February 1998         
                    </div>
                  </div>
                </div><div class="col-sm-3">
                  <div >
                    <img src="{{asset('img/member/piam.png')}}"> 
                    <div class="nameMem">
                      PIAM              
                    </div>
                    <div class="positionMem">
                    </div>
                    <div class="briMem">
                      4 June 2003        
                    </div>
                  </div>
                </div><div class="col-sm-3">
                  <div >
                    <img src="{{asset('img/member/pun.png')}}"> 
                    <div class="nameMem">
                      PUN              
                    </div>
                    <div class="positionMem">
                    </div>
                    <div class="briMem">
                      9 November 2000    
                    </div>
                  </div>
                </div><div class="col-sm-3">
                  <div >
                    <img src="{{asset('img/member/pupe.png')}}"> 
                    <div class="nameMem">
                      PUPE              
                    </div>
                    <div class="positionMem">
                    </div>
                    <div class="briMem">
                      18 January 1998          
                    </div>
                  </div>
                </div><div class="col-sm-3">
                  <div >
                    <img src="{{asset('img/member/ratah.png')}}"> 
                    <div class="nameMem">
                      RATAH              
                    </div>
                    <div class="positionMem">
                    </div>
                    <div class="briMem">
                      27 March  2000          
                    </div>
                  </div>
                </div><div class="col-sm-3">
                  <div >
                    <img src="{{asset('img/member/satchan.png')}}"> 
                    <div class="nameMem">
                      SATCHAN              
                    </div>
                    <div class="positionMem">
                    </div>
                    <div class="briMem">
                      19 December 2003   
                    </div>
                  </div>
                </div><div class="col-sm-3">
                  <div >
                    <img src="{{asset('img/member/stang.png')}}"> 
                    <div class="nameMem">
                      STANG              
                    </div>
                    <div class="positionMem">
                    </div>
                    <div class="briMem">
                      22 October 2003  
                    </div>
                  </div>
                </div>
              </div>
            </div> </p>
          </blockquote>
          <div class="col">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 divMemMembers">
              <div class="boxMemx" role="52">
                <div class="row justify-content-center">
                  <!-- <img  width="45" height="54" src="img/digitallive.jpg">  -->
                  <div class="">
                    <div class="row">
                      <div class="col-sm-3">
                        <div >
                         <img src="{{asset('img/member/tarwaan.png')}}"> 
                         <div class="nameMem">
                          TARWAAN              
                        </div>
                        <div class="positionMem">
                        </div>
                        <div class="briMem">
                          18 December 1996   
                        </div>
                      </div>
                    </div><div class="col-sm-3">
                      <div >
                       <img src="{{asset('img/member/view.png')}}"> 
                       <div class="nameMem">
                        VIEW              
                      </div>
                      <div class="positionMem">
                      </div>
                      <div class="briMem">
                        28 May 2004   
                      </div>
                    </div>
                  </div><div class="col-sm-3">
                    <div >
                      <img src="{{asset('img/member/wee.png')}}"> 
                      <div class="nameMem">
                        WEE              
                      </div>
                      <div class="positionMem">
                      </div>
                      <div class="briMem">
                        23 October 2001          
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div>
      
<!-- Vendor -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
<script src="vendor/popper/umd/popper.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/common/common.min.js"></script>
<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="vendor/isotope/jquery.isotope.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="vendor/vide/vide.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Theme Custom -->
<script src="js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="js/theme.init.js"></script>

    <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-12345678-1', 'auto');
      ga('send', 'pageview');
    </script>
  -->

</body>
</html>

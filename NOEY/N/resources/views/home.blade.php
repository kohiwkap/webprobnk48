<!DOCTYPE html>
<!-- saved from url=(0041)https://www.bnk48.com/index.php?page=home -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>BNK48 Official Website</title>
  <link rel="icon" type="image/png" sizes="32x32" href="https://www.bnk48.com/plugins/images/favicon-32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="https://www.bnk48.com/plugins/images/favicon-16.png">
  <link rel="stylesheet" href="./BNK48 Official Website_files/bootstrap.min.css">
  <link rel="stylesheet" href="./BNK48 Official Website_files/bnk48main.css">
  <link rel="stylesheet" href="./BNK48 Official Website_files/fonts.css">
  <link rel="stylesheet" href="./BNK48 Official Website_files/font-awesome.css">
  <link rel="stylesheet" href="./BNK48 Official Website_files/bootstrap-select.min.css">
  <link rel="stylesheet" href="./BNK48 Official Website_files/slick.css">
  <link rel="stylesheet" href="./BNK48 Official Website_files/slick-theme.css">

  <script type="text/javascript" async="" src="./BNK48 Official Website_files/analytics.js.ดาวน์โหลด"></script><script type="text/javascript" src="./BNK48 Official Website_files/jquery.min.js.ดาวน์โหลด"></script>
  <script type="text/javascript" src="./BNK48 Official Website_files/bootstrap.min.js.ดาวน์โหลด"></script>
  <script type="text/javascript" src="./BNK48 Official Website_files/bootstrap-select.min.js.ดาวน์โหลด"></script>
  <script type="text/javascript" src="./BNK48 Official Website_files/vue-carousel-3d.min.js.ดาวน์โหลด"></script><style type="text/css">.carousel-3d-container[data-v-c06c963c]{min-height:1px;width:100%;position:relative;z-index:0;overflow:hidden;margin:20px auto;box-sizing:border-box}.carousel-3d-slider[data-v-c06c963c]{position:relative;margin:0 auto;transform-style:preserve-3d;-webkit-perspective:1000px;-moz-perspective:1000px;perspective:1000px}</style><style type="text/css">.carousel-3d-controls[data-v-43e93932]{position:absolute;top:50%;height:0;margin-top:-30px;left:0;width:100%;z-index:1000}.next[data-v-43e93932],.prev[data-v-43e93932]{width:60px;position:absolute;z-index:1010;font-size:60px;height:60px;line-height:60px;color:#333;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;text-decoration:none;top:0}.next[data-v-43e93932]:hover,.prev[data-v-43e93932]:hover{cursor:pointer;opacity:.7}.prev[data-v-43e93932]{left:10px;text-align:left}.next[data-v-43e93932]{right:10px;text-align:right}.disabled[data-v-43e93932],.disabled[data-v-43e93932]:hover{opacity:.2;cursor:default}</style><style type="text/css">.carousel-3d-slide{position:absolute;opacity:0;visibility:hidden;overflow:hidden;top:0;border-radius:1px;border-color:#000;border-color:rgba(0,0,0,.4);border-style:solid;background-size:cover;background-color:#ccc;display:block;margin:0;box-sizing:border-box;text-align:left}.carousel-3d-slide img{width:100%}.carousel-3d-slide.current{opacity:1!important;visibility:visible!important;transform:none!important;z-index:999}</style>
  <script type="text/javascript" src="./BNK48 Official Website_files/vue.js.ดาวน์โหลด"></script>
  <script type="text/javascript" src="./BNK48 Official Website_files/main.js.ดาวน์โหลด"></script>
  <script type="text/javascript" src="./BNK48 Official Website_files/slick.js.ดาวน์โหลด"></script>
  <script type="text/javascript">
    var is_safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if(is_safari){
      $('head').append(
        $('<link rel="stylesheet" type="text/css" href="/plugins/css/bnk48safari.css?v=6" />')
        );
    }

    var mac = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i) ? true : false;
    if(!mac){
      $('head').append(
        $('<link rel="stylesheet" type="text/css" href="/plugins/css/bnk48windows.css" />')
        );      
      </script>
      <!-- <link rel="stylesheet" type="text/css" href="./BNK48 Official Website_files/bnk48windows.css"> -->

      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async="" src="./BNK48 Official Website_files/js"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-128405141-1');
      </script>


    </head>
    <body>
      <div id="loading" style="display: none;">
        <div class="spinner">
          <div class="bounce1"></div>
          <div class="bounce2"></div>
          <div class="bounce3"></div>
        </div>
      </div>
      <div id="container">
       <div id="header">
         <div class="container">
          <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
              <div class="logosizemobile" style="display:none">
                <a href="https://www.bnk48.com/index.php?page=home"><img src="./BNK48 Official Website_files/bnk48logo2.png" alt=""></a>
              </div>
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav cl-effect-16">
                  <li class="hoverli current">
                    <a class="" href="https://www.bnk48.com/index.php?page=home" data-hover="Home">Home</a>
                  </li>
                  <li class="hoverli ">
                    <a class="" href="https://www.bnk48.com/index.php?page=members" data-hover="Members">Members</a>
                  </li>
                  <li class="hoverli ">
                    <a class="" href="https://www.bnk48.com/index.php?page=schedule" data-hover="Schedule">Schedule</a>
                  </li>
                  <li class="licenterbnk48"><a class="centerbnk48"></a></li>
                  <li class="hoverli ">
                    <a class="" href="https://www.bnk48.com/index.php?page=news" data-hover="News">News</a>
                  </li>
                  <li class="hoverli">
                    <a class="" href="https://ticket.bnk48.com/" target="_blank" data-hover="Ticket">Ticket</a>
                  </li>
                  <li class="hoverli">
                    <a class="" href="https://shop.bnk48.com/" target="_blank" data-hover="Shop">Shop</a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>

        <style>
/* li a .hoverclass:before {
    content: '';
    display: block;
    position: absolute;
    top: calc(100% + 3px);
    left: 0;
    width: 100%;
    height: 1px;
    z-index: 1000;
    opacity: 0;
    background-color: currentColor;
    -webkit-transform: scaleX(0);
    -ms-transform: scaleX(0);
    transform: scaleX(0);
    transition: 0.4s;
}

li:hover > a .hoverclass:before {
  opacity: 1;
  width: 100%;
  -webkit-transform: scaleX(1);
      -ms-transform: scaleX(1);
          transform: scaleX(1);
}

li a .hoverclass{
  position: relative;
  } */




</style>
</div>
<div id="body">

 <div class="bgHome">
  <!--  start slider banner  -->
  <div id="carousel3d"><div data-v-c06c963c="" class="carousel-3d-container" style="height: 402px;"><div data-v-c06c963c="" class="carousel-3d-slider" style="width: 872px; height: 402px;"><div class="carousel-3d-slide right-1" data-v-c06c963c="" style="border-width: 1px; width: 872px; height: 402px; transition: transform 500ms ease 0s, opacity 500ms ease 0s, visibility 500ms ease 0s; transform: translateX(-400px) translateZ(-400px) rotateY(0deg); z-index: 998; opacity: 1; visibility: visible;"><a href="https://www.bnk48.com/" target="_blank"><img src="./BNK48 Official Website_files/113351djlny8.png" alt=""></a></div> <div class="carousel-3d-slide left-1" data-v-c06c963c="" style="border-width: 1px; width: 872px; height: 402px; transition: transform 500ms ease 0s, opacity 500ms ease 0s, visibility 500ms ease 0s; transform: translateX(400px) translateZ(-400px) rotateY(-0deg); z-index: 998; opacity: 1; visibility: visible;"><a href="https://itunes.apple.com/th/album/bnk-festival-ep/1446555570" target="_blank"><img src="./BNK48 Official Website_files/092635cfz578.png" alt=""></a></div> <div class="carousel-3d-slide current" data-v-c06c963c="" style="border-width: 1px; width: 872px; height: 402px; transition: transform 500ms ease 0s, opacity 500ms ease 0s, visibility 500ms ease 0s;"><a href="https://election.bnk48.com/" target="_blank"><img src="./BNK48 Official Website_files/091338aflnt6.png" alt=""></a></div></div> <!----></div></div>
</div>

<!--  end slider banner  -->

<div class="container containerbg">
  <!-- start News -->
  <div class="txtNews">
    <div class="row Newsx nomargin">
      <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 nopadding">
        <span class="txtNewshr">News</span>
        <hr class="hrclass">
      </div>

      <div class="col-xs-3 col-sm-2 col-md-2 col-lg-2 nopadding">
        <img class="imgborder" src="./BNK48 Official Website_files/group-4.png" alt="">
      </div>
    </div>
  </div>

  <div class="boxNews">
    <div class="row newsboxbg">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 bdrightNews">
        <div class="txtbnk48update">
          <span class="txthrbnk48update">BNK48 UPDATE</span>
          <a href="https://www.bnk48.com/index.php?page=news"><span class="viewall48update"> View More </span></a>
        </div>
        <div class="boxNewsDetail">
          <a href="https://www.bnk48.com/index.php?page=newslist&amp;newsId=27">
            <div class="boxNewsDetails">
              <div class="dateBox">
                <img src="./BNK48 Official Website_files/103632qstx36.jpg" alt="">
                
              </div>
              <div class="boxdetailNews">
                <div class="offNews">
                  <span class="offnewsbox">OFFICIAL NEWS</span><span class="newsnoti">NEWS !</span>
                </div>
                <div class="nameNews" title="Digital Live Studio [13 ธันวาคม 2561 2561] กับแขกรับเชิญสุดพิเศษ ใครกันนะ???">
                Digital Live Studio [13 ธันวาคม 2561 2561] กับแขกรับเชิญสุดพิเศษ ใครกันนะ???                </div>
                <div class="dateNews">
                  13 Dec  เวลา 18:00 น. - 19:00 น.
                </div>
              </div>

            </div>
          </a>
          <div class="hrBox"></div>
          <a href="https://www.bnk48.com/index.php?page=newslist&amp;newsId=26">
            <div class="boxNewsDetails">
              <div class="dateBox">
                <img src="./BNK48 Official Website_files/085438cdjxy2.png" alt="">
                
              </div>
              <div class="boxdetailNews">
                <div class="offNews">
                  <span class="offnewsbox">OFFICIAL NEWS</span><span class="newsnoti">NEWS !</span>
                </div>
                <div class="nameNews" title="มาแล้ว !! สรุปผลด่วน BNK48 6th Single Senbatsu General Election">
                มาแล้ว !! สรุปผลด่วน BNK48 6th Single Senbatsu General Election                </div>
                <div class="dateNews">
                  12 Dec  เวลา 19:00 น. - 20:00 น.
                </div>
              </div>

            </div>
          </a>
          <div class="hrBox"></div>
          <a href="https://www.bnk48.com/index.php?page=newslist&amp;newsId=24">
            <div class="boxNewsDetails">
              <div class="dateBox">
                <img src="./BNK48 Official Website_files/014618egnsy7.png" alt="">
                
              </div>
              <div class="boxdetailNews">
                <div class="offNews">
                  <span class="offnewsbox">OFFICIAL NEWS</span><span class="newsnoti">NEWS !</span>
                </div>
                <div class="nameNews" title="【BNK48 The Campus Stage Performance - วันที่ 14 - 16 DEC. 2018】">
                【BNK48 The Campus Stage Performance - วันที่ 14 - 16 DEC. 2018】                </div>
                <div class="dateNews">
                  10 Dec  เวลา 21:00 น. - 22:00 น.
                </div>
              </div>

            </div>
          </a>
          <div class="hrBox"></div>
          <a href="https://www.bnk48.com/index.php?page=newslist&amp;newsId=25">
            <div class="boxNewsDetails">
              <div class="dateBox">
                <img src="./BNK48 Official Website_files/020224cgks24.png" alt="">
                
              </div>
              <div class="boxdetailNews">
                <div class="offNews">
                  <span class="offnewsbox">OFFICIAL NEWS</span><span class="newsnoti">NEWS !</span>
                </div>
                <div class="nameNews" title="หกรรมรวมพลคนรัก BNK48 กับงาน BNK48 Fan Festival 2018 ">
                หกรรมรวมพลคนรัก BNK48 กับงาน BNK48 Fan Festival 2018                 </div>
                <div class="dateNews">
                  7 Dec  เวลา 20:45 น. - 21:45 น.
                </div>
              </div>

            </div>
          </a>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 bdleftNews">
        <div class="txtbnk48update">
          <span class="txthrbnk48update">SCHEDULE</span>
          <a href="https://www.bnk48.com/index.php?page=schedule"><span class="viewall48update"> View More </span></a>
        </div>
        <div class="boxNewsDetail">
          <a href="https://www.bnk48.com/index.php?page=schedule&amp;schId=374">
            <div class="boxNewsDetailsx">
              <div class="dateBoxs">
                <div class="txt1">
                17/12                </div>
                <div class="txt2">
                Mon/2018                </div>
              </div>
              <div class="boxdetailNews">
                <div class="schtxteven" title="Ratah Name">
                Ratah Name                </div>
                <!-- <div class="viewdetailschbox">
                    <a href="?page=schedule&schId=374"><span class="viewdetailschtxt" role="374">View Detail</span></a>
                  </div> -->
                </div>
              </div>
            </a>
            <div class="hrBox"></div>
            <a href="https://www.bnk48.com/index.php?page=schedule&amp;schId=444">
              <div class="boxNewsDetailsx">
                <div class="dateBoxs">
                  <div class="txt1">
                  17/12                </div>
                  <div class="txt2">
                  Mon/2018                </div>
                </div>
                <div class="boxdetailNews">
                  <div class="schtxteven" title="Mobile Name">
                  Mobile Name                </div>
                <!-- <div class="viewdetailschbox">
                    <a href="?page=schedule&schId=444"><span class="viewdetailschtxt" role="444">View Detail</span></a>
                  </div> -->
                </div>
              </div>
            </a>
            <div class="hrBox"></div>
            <a href="https://www.bnk48.com/index.php?page=schedule&amp;schId=431">
              <div class="boxNewsDetailsx">
                <div class="dateBoxs">
                  <div class="txt1">
                  10/02                </div>
                  <div class="txt2">
                  Sun/2019                </div>
                </div>
                <div class="boxdetailNews">
                  <div class="schtxteven" title="Event : วิ่งข้ามสมุทร 2019">
                  Event : วิ่งข้ามสมุทร 2019                </div>
                <!-- <div class="viewdetailschbox">
                    <a href="?page=schedule&schId=431"><span class="viewdetailschtxt" role="431">View Detail</span></a>
                  </div> -->
                </div>
              </div>
            </a>
            <div class="hrBox"></div>
            <a href="https://www.bnk48.com/index.php?page=schedule&amp;schId=254">
              <div class="boxNewsDetailsx">
                <div class="dateBoxs">
                  <div class="txt1">
                  26/01                </div>
                  <div class="txt2">
                  Sat/2019                </div>
                </div>
                <div class="boxdetailNews">
                  <div class="schtxteven" title="ประกาศผล BNK48 6th Single Senbatsu General Election">
                  ประกาศผล BNK48 6th Single Senbatsu General Election                </div>
                <!-- <div class="viewdetailschbox">
                    <a href="?page=schedule&schId=254"><span class="viewdetailschtxt" role="254">View Detail</span></a>
                  </div> -->
                </div>
              </div>
            </a>
            <div class="hrBox"></div>
            <a href="https://www.bnk48.com/index.php?page=schedule&amp;schId=411">
              <div class="boxNewsDetailsx">
                <div class="dateBoxs">
                  <div class="txt1">
                  19/01                </div>
                  <div class="txt2">
                  Sat/2019                </div>
                </div>
                <div class="boxdetailNews">
                  <div class="schtxteven" title="Trang music festival">
                  Trang music festival                </div>
                <!-- <div class="viewdetailschbox">
                    <a href="?page=schedule&schId=411"><span class="viewdetailschtxt" role="411">View Detail</span></a>
                  </div> -->
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>

    <!-- end News -->

    <div class="txtNews">
      <div class="row Performancex nomargin">
        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 nopadding">
          <span class="txtNewshr">Performance(s)</span>
          <hr class="hrclass">
        </div>
        <div class="col-xs-3 col-sm-2 col-md-2 col-lg-2 nopadding">
          <img class="imgborder" src="./BNK48 Official Website_files/group-4.png" alt="">
        </div>
      </div>
    </div>
    <div class="boxPer">
      <div class="well">
        <div class="slider-header slick-initialized slick-slider">

          <div aria-live="polite" class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 180px; transform: translate3d(0px, 0px, 0px);" role="listbox"><div class="boxperitem slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" style="width: 170px;" tabindex="-1" role="option" aria-describedby="slick-slide00">
            <a href="https://ticket.bnk48.com/index.php?page=theater&amp;theater=88" target="_blank" tabindex="0">
              <img src="./BNK48 Official Website_files/20181217021448abdqtuxy3469.jpg" alt="Image" class="img-responsive">
              <div class="dateper">
              Date 21 December 2018            </div>
              <div class="timeper">
                Time <span class="green">19:00</span>
                <span><i class="fa fa-check-circle" aria-hidden="true"></i></span>
              </div>
            </a>
          </div></div></div>
          
        </div>
      </div>
    </div>

    <div class="boxYoutube">
      <div class="iconYoutube"><i class="fa fa-youtube-play" aria-hidden="true"></i></div>
      <div class="txtYoutube">
        <div class="textYouoff">YOUTUBE OFFICIAL</div>
        <a href="https://www.youtube.com/channel/UClIsaGq7vBEW00ASqwQyzPw" target="_blank">
          <div class="textYouoffbox">YOUTUBE OFFICIAL</div>
        </a>
        <div class="hrYouoff"><hr class="hrclassoff"></div>
      </div>
      <div class="row youtubeBox">
        <div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 nopadding-right">
          <div class="youtubeBox1">
            <iframe width="700" height="400" src="./BNK48 Official Website_files/mo4l8f_VZXo.html" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
          </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
          <div class="youtubeBox2">
            <iframe width="210" height="123" src="./BNK48 Official Website_files/iByxlVvWrww.html" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
          </div>
          <div class="youtubeBox3">
            <iframe width="210" height="123" src="./BNK48 Official Website_files/su_ZmVg0nw0.html" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
          </div>
          <div class="youtubeBox4">
            <iframe width="210" height="123" src="./BNK48 Official Website_files/QkYPSuLYXpM.html" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
          </div>
        </div>
      </div>
      <div class="row boxImgyouoff">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
          <a href="https://ticket.bnk48.com/" target="_blank">
            <div class="boxImgyou">
              <img class="imgYouoff" src="./BNK48 Official Website_files/040155chjlrs.png" alt="">
              <!-- <img class="imgYouoff" src="/plugins/images/Ticket/BNK_Ticket.svg" alt=""> -->

            </div>
          </a>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 nopadding">
          <a href="https://election.bnk48.com/" target="_blank">
            <div class="boxImgyou">
              <img class="imgYouoff" src="./BNK48 Official Website_files/092850egou37.png" alt="">
              <!-- <img class="imgYouoff" src="/plugins/images/FAQ/BNK_FAQ.svg" alt=""> -->
            </div>
          </a>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
          <a href="https://shop.bnk48.com/" target="_blank">
            <div class="boxImgyou">
              <img class="imgYouoff" src="./BNK48 Official Website_files/123812ijnw46.png" alt="">
              <!-- <img class="imgYouoff" src="/plugins/images/Single/BNK_Single.svg" alt=""> -->
            </div>
          </a>
        </div>
      </div>
    </div>

    <div class="txtNews">
      <div class="row Membersx nomargin">
        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 nopadding">
          <span class="txtNewshr">Members</span>
          <hr class="hrclass">
        </div>
        <div class="col-xs-3 col-sm-2 col-md-2 col-lg-2 nopadding">
          <img class="imgborder" src="./BNK48 Official Website_files/group-4.png" alt="">
        </div>
      </div>
    </div>
    <div class="boxMembers">
      <div class="member-slide slick-initialized slick-slider"><button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button" style="">Previous</button>
        <div aria-live="polite" class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 6919px; transform: translate3d(-4675px, 0px, 0px);" role="listbox"><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-cloned" data-slick-index="-6" aria-hidden="true" style="width: 187px;" tabindex="-1">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=3" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/3/s/20181123093629ghtuv5.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                ORN            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=32" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/32/s/20181123092613gos567.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                PAKWAN            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-cloned" data-slick-index="-5" aria-hidden="true" style="width: 187px;" tabindex="-1">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=31" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/31/s/20181123092634cdefru.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                PANDA            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=30" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/30/s/20181123092657ghptv5.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                PHUKKHOM            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-cloned" data-slick-index="-4" aria-hidden="true" style="width: 187px;" tabindex="-1">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=8" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/8/s/20181123093439aimos2.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                PIAM            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=1" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/1/s/20181123093714fgmz14.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                PUN            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-cloned" data-slick-index="-3" aria-hidden="true" style="width: 187px;" tabindex="-1">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=7" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/7/s/20181123093500kmsx57.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                PUPE            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=29" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/29/s/20181123092722ehivw5.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                RATAH            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-cloned" data-slick-index="-2" aria-hidden="true" style="width: 187px;" tabindex="-1">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=6" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/6/s/20181123093532acdrs6.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                SATCHAN            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=28" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/28/s/20181123092742bdjtu3.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                STANG            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-cloned" data-slick-index="-1" aria-hidden="true" style="width: 187px;" tabindex="-1">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=5" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/5/s/20181123093550ceoswy.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                TARWAAN            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=27" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/27/s/20181123092802del457.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                VIEW            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="0" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide10">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=52" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/52/s/20181123091728bcfhk5.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                AOM            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=51" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/51/s/20181123091846grv159.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                BAMBOO            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="1" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide11">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=50" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/50/s/20181123091902cimrw9.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                CAKE            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=4" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/4/s/20181123093609prtvwy.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                CHERPRANG            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="2" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide12">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=49" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/49/s/20181123091918cdkos7.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                DEENEE            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=48" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/48/s/20181123091933efw249.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                FAII            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="3" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide13">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=47" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/47/s/20181123091949dw2456.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                FIFA            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=46" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/46/s/20181123092006adkv45.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                FOND            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="4" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide14">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=45" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/45/s/20181123092024eimpv1.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                GYGEE            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=25" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/25/s/20181123092844eikqx9.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                IZURINA            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="5" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide15">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=23" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/23/s/20181123092921bfjp46.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                JAA            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=24" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/24/s/20181123092904lvyz35.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                JANE            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="6" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide16">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=2" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/2/s/20181123093652ceisz1.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                JENNIS            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=22" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/22/s/20181123092940adjpx7.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                JIB            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="7" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide17">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=44" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/44/s/20181123092103bgy126.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                JUNÉ            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=21" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/21/s/20181123092956ikpt69.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                KAEW            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="8" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide18">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=20" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/20/s/20181123093013lsw348.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                KAIMOOK            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=19" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/19/s/20181123093035fhtvz3.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                KATE            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="9" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide19">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=43" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/43/s/20181123092143hloqsu.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                KHAMIN            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=42" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/42/s/20181123092218glou89.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                KHENG            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="10" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide110">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=18" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/18/s/20181123093052ckr368.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                KORN            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=41" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/41/s/20181123092240bglmt3.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                MAIRA            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="11" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide111">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=40" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/40/s/20181123092304eglsz2.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                MEWNICH            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=16" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/16/s/20181123093116fnsw27.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                MIND            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="12" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide112">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=39" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/39/s/20181123092324cdqs15.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                MINMIN            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=15" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/15/s/20181123093136abez56.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                MIORI            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="13" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide113">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=14" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/14/s/20181123093154bcsuv9.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                MOBILE            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=13" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/13/s/20181123093213adej67.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                MUSIC            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="14" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide114">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=38" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/38/s/20181123092347cfw568.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                MYYU            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=12" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/12/s/20181123093240ioxy14.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                NAMNEUNG            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="15" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide115">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=11" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/11/s/20181123093259mqst29.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                NAMSAI            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=37" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/37/s/20181123092429htuy28.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                NATHERINE            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="16" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide116">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=36" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/36/s/20181123092449cfilyz.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                NEW            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=35" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/35/s/20181123092509cfimns.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                NIKY            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="17" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide117">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=34" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/34/s/20181123092529ekruy5.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                NINE            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=10" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/10/s/20181123093355cfpu38.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                NINK            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide" data-slick-index="18" aria-hidden="true" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide118">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=9" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/9/s/20181123093415cdghm9.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                NOEY            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=33" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/33/s/20181123092548fqstv6.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                OOM            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-active" data-slick-index="19" aria-hidden="false" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide119">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=3" tabindex="0">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/3/s/20181123093629ghtuv5.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                ORN            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=32" tabindex="0">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/32/s/20181123092613gos567.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                PAKWAN            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-active" data-slick-index="20" aria-hidden="false" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide120">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=31" tabindex="0">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/31/s/20181123092634cdefru.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                PANDA            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=30" tabindex="0">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/30/s/20181123092657ghptv5.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                PHUKKHOM            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-active" data-slick-index="21" aria-hidden="false" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide121">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=8" tabindex="0">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/8/s/20181123093439aimos2.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                PIAM            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=1" tabindex="0">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/1/s/20181123093714fgmz14.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                PUN            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-active" data-slick-index="22" aria-hidden="false" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide122">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=7" tabindex="0">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/7/s/20181123093500kmsx57.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                PUPE            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=29" tabindex="0">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/29/s/20181123092722ehivw5.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                RATAH            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-active" data-slick-index="23" aria-hidden="false" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide123">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=6" tabindex="0">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/6/s/20181123093532acdrs6.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                SATCHAN            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=28" tabindex="0">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/28/s/20181123092742bdjtu3.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                STANG            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-current slick-active" data-slick-index="24" aria-hidden="false" style="width: 187px;" tabindex="-1" role="option" aria-describedby="slick-slide124">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=5" tabindex="0">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/5/s/20181123093550ceoswy.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                TARWAAN            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=27" tabindex="0">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/27/s/20181123092802del457.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                VIEW            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-cloned" data-slick-index="25" aria-hidden="true" style="width: 187px;" tabindex="-1">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=52" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/52/s/20181123091728bcfhk5.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                AOM            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=51" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/51/s/20181123091846grv159.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                BAMBOO            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-cloned" data-slick-index="26" aria-hidden="true" style="width: 187px;" tabindex="-1">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=50" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/50/s/20181123091902cimrw9.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                CAKE            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=4" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/4/s/20181123093609prtvwy.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                CHERPRANG            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-cloned" data-slick-index="27" aria-hidden="true" style="width: 187px;" tabindex="-1">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=49" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/49/s/20181123091918cdkos7.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                DEENEE            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=48" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/48/s/20181123091933efw249.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                FAII            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-cloned" data-slick-index="28" aria-hidden="true" style="width: 187px;" tabindex="-1">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=47" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/47/s/20181123091949dw2456.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                FIFA            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=46" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/46/s/20181123092006adkv45.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                FOND            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-cloned" data-slick-index="29" aria-hidden="true" style="width: 187px;" tabindex="-1">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=45" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/45/s/20181123092024eimpv1.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                GYGEE            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=25" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/25/s/20181123092844eikqx9.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                IZURINA            </div>
              </div>
            </div>
          </a>

        </div><div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 divMemhome slick-slide slick-cloned" data-slick-index="30" aria-hidden="true" style="width: 187px;" tabindex="-1">
          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=23" tabindex="-1">
            <div class="boxMemx">
              <div class="ImgMem" style="background-image: url(data/Members/23/s/20181123092921bfjp46.png);"></div>
              <div class="boxnameMemhome">
                <div class="nameMemhome">
                JAA            </div>
              </div>
            </div>
          </a>

          <a href="https://www.bnk48.com/index.php?page=listMembers&amp;memberId=24" tabindex="-1">
            <div class="boxMemxL">
              <div class="ImgMemL" style="background-image: url(data/Members/24/s/20181123092904lvyz35.png);"></div>
              <div class="boxnameMemhomeL">
                <div class="nameMemhomeL">
                JANE            </div>
              </div>
            </div>
          </a>

        </div></div></div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        <button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="">Next</button></div>
      </div>

      <div class="txtNews">
        <div class="row Socialx nomargin">
          <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 nopadding">
            <span class="txtNewshr">Social Timeline</span>
            <hr class="hrclass">
          </div>
          <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 nopadding">
            <img class="imgborder" src="./BNK48 Official Website_files/group-4.png" alt="">
          </div>
        </div>
      </div>
      <div class="boxSocial">
        <input type="hidden" name="seMemIns" value="">
        <div class="row homeInstagram">
          <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 divinstar nopadding">
            <div class="boxSocialx">
              <img class="ImgSocial" src="./BNK48 Official Website_files/group-3@2x.jpg" alt="">
              <div class="selectMem"></div>
            </div>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 divinstar nopadding">
            <input type="hidden" name="memIns[]" value="3047">
            <a href="https://www.instagram.com/p/Bre5wJMhzFc" target="_blank">
              <div class="boxSocialx">
                <div class="ImgSocial" style="background-image: url(https://instagram.fbkk10-1.fna.fbcdn.net/vp/5eafb8b402572347cb3ea443b4de4d42/5C98013B/t51.2885-15/e35/s320x320/46748300_454933061703150_6289415211546208397_n.jpg);"></div>
                <div class="boxNameIns">AOM.BNK48OFFICIAL</div>
              </div>
            </a>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 divinstar nopadding">
            <input type="hidden" name="memIns[]" value="3046">
            <a href="https://www.instagram.com/p/BrfIgRznAlX" target="_blank">
              <div class="boxSocialx">
                <div class="ImgSocial" style="background-image: url(https://instagram.fbkk10-1.fna.fbcdn.net/vp/76fb0d4c9f620b1160b608bf81092f1d/5CD8986D/t51.2885-15/e35/s320x320/45774125_2307791702812352_8567788967216636477_n.jpg?_nc_ht=instagram.fbkk10-1.fna.fbcdn.net);"></div>
                <div class="boxNameIns">KHENG.BNK48OFFICIAL</div>
              </div>
            </a>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 divinstar nopadding">
            <input type="hidden" name="memIns[]" value="3045">
            <a href="https://www.instagram.com/p/BrfI1g2HjvP" target="_blank">
              <div class="boxSocialx">
                <div class="ImgSocial" style="background-image: url(https://instagram.fbkk10-1.fna.fbcdn.net/vp/12cf6d7cd9b3bdcf0eeb8c96f3ac9bf0/5C94BA87/t51.2885-15/e35/s320x320/46841159_603971953760223_6107211748072235106_n.jpg?_nc_ht=instagram.fbkk10-1.fna.fbcdn.net);"></div>
                <div class="boxNameIns">KHENG.BNK48OFFICIAL</div>
              </div>
            </a>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 divinstar nopadding">
            <input type="hidden" name="memIns[]" value="3044">
            <a href="https://www.instagram.com/p/BrfRV14FurC" target="_blank">
              <div class="boxSocialx">
                <div class="ImgSocial" style="background-image: url(https://instagram.fbkk10-1.fna.fbcdn.net/vp/375200cca0e1dd36cc22b2be0090e561/5CA8F081/t51.2885-15/e35/c19.0.1042.1042/s320x320/47183230_280336055999705_2009203862122503247_n.jpg?_nc_ht=instagram.fbkk10-1.fna.fbcdn.net);"></div>
                <div class="boxNameIns">NAMNEUNG.BNK48OFFICIAL</div>
              </div>
            </a>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 divinstar nopadding">
            <input type="hidden" name="memIns[]" value="3043">
            <a href="https://www.instagram.com/p/BrfDH5IhZ0s" target="_blank">
              <div class="boxSocialx">
                <div class="ImgSocial" style="background-image: url(https://instagram.fbkk10-1.fna.fbcdn.net/vp/3fbedabc451ec0d8bdb7685d4ae56c85/5CA8A11A/t51.2885-15/e35/c179.0.721.721/s320x320/47691208_571777833271184_9002464214444368559_n.jpg);"></div>
                <div class="boxNameIns">NAMSAI.BNK48OFFICIAL</div>
              </div>
            </a>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 divinstar nopadding">
            <input type="hidden" name="memIns[]" value="3042">
            <a href="https://www.instagram.com/p/BrfG6odH6wC" target="_blank">
              <div class="boxSocialx">
                <div class="ImgSocial" style="background-image: url(https://instagram.fbkk10-1.fna.fbcdn.net/vp/29d0be280188745dc77a59c83749ec09/5CA8328B/t51.2885-15/e35/c0.111.890.890a/s320x320/45514690_2152769548322228_1761591214323529616_n.jpg?_nc_ht=instagram.fbkk10-1.fna.fbcdn.net);"></div>
                <div class="boxNameIns">TARWAAN.BNK48OFFICIAL</div>
              </div>
            </a>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 divinstar nopadding">
            <input type="hidden" name="memIns[]" value="3041">
            <a href="https://www.instagram.com/p/BrfOhqZF7_0" target="_blank">
              <div class="boxSocialx">
                <div class="ImgSocial" style="background-image: url(https://instagram.fbkk10-1.fna.fbcdn.net/vp/8fc8af61b25ab93993c4ec8f12a4da88/5CA9C48D/t51.2885-15/e35/s320x320/46338093_752840248415038_204384960451057086_n.jpg?_nc_ht=instagram.fbkk10-1.fna.fbcdn.net);"></div>
                <div class="boxNameIns">CAKE.BNK48OFFICIAL</div>
              </div>
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="loadmore">
              <button class="btn btn-default LoadMoreIns" type="button" name="button">Load More</button>
            </div>
          </div>
        </div>
      </div>
      <div class="row boxAss">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 textcen">
          <!-- <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"> -->
            <div class="boxAssImg">
              <a href="https://www.facebook.com/bnk48official/" target="_blank">
                <img class="" src="./BNK48 Official Website_files/113741ery145.png" alt="">
                <!-- <img class="boxAssImgx" src="/plugins/images/youtube/BNK_BNKyoutube.svg" alt=""> -->
              </a>
            </div>
            <!-- </div> -->
            <!-- <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"> -->
              <div class="boxAssImg">
                <a href="https://www.youtube.com/channel/UClIsaGq7vBEW00ASqwQyzPw?ab_channel=BNK48" target="_blank">
                  <img src="./BNK48 Official Website_files/033145grs459.png" alt="">
                  <!-- <img src="/plugins/images/FB/BNK_BNKFB.svg" alt="" class="boxAssImgx"> -->
                </a>
              </div>
              <!-- </div> -->
              <!-- <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"> -->
                <div class="boxAssImg">
                  <a href="https://twitter.com/bnk48official/" target="_blank">
                    <img class="" src="./BNK48 Official Website_files/113617fhorx9.png" alt="">
                    <!-- <img class="boxAssImgx" src="/plugins/images/Shop/BNK_BNKShop.svg" alt="" > -->
                  </a>
                </div>
                <!-- </div> -->
              </div>
            </div>

            <div class="row group48">
              <div class="col-xs-3 col-sm-4 col-md-4 col-lg-5">
                <hr class="hrclass48group">
              </div>
              <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2">
                <div class="grouptxt48">48 GROUP</div>
              </div>
              <div class="col-xs-3 col-sm-4 col-md-4 col-lg-5">
                <hr class="hrclass48group">
              </div>
            </div>

            <div class="row group48Imgcen">
              <a href="https://www.akb48.co.jp/" target="_blank">
                <div class="group48Img"><img src="./BNK48 Official Website_files/035847mz1245.png" alt=""></div>
              </a>
              <a href="https://www.facebook.com/AKB48TeamTP" target="_blank">
                <div class="group48Img"><img src="./BNK48 Official Website_files/035916ejtvx7.png" alt=""></div>
              </a>
              <a href="https://jkt48.com/" target="_blank">
                <div class="group48Img"><img src="./BNK48 Official Website_files/035928aly158.png" alt=""></div>
              </a>
              <a href="http://www.hkt48.jp/" target="_blank">
                <div class="group48Img"><img src="./BNK48 Official Website_files/035937mosxz6.png" alt=""></div>
              </a>
              <a href="https://mnl48.hallohallo.com/" target="_blank">
                <div class="group48Img"><img src="./BNK48 Official Website_files/035947dhjmsx.png" alt=""></div>
              </a>
              <br>
              <a href="http://ngt48.jp/" target="_blank">
                <div class="group48Img"><img src="./BNK48 Official Website_files/035957egms39.png" alt=""></div>
              </a>
              <a href="http://www.nmb48.com/" target="_blank">
                <div class="group48Img"><img src="./BNK48 Official Website_files/040007buy456.png" alt=""></div>
              </a>
              <a href="http://www.ske48.co.jp/" target="_blank">
                <div class="group48Img"><img src="./BNK48 Official Website_files/040017behkz3.png" alt=""></div>
              </a>
              <a href="http://www.stu48.com/" target="_blank">
                <div class="group48Img"><img src="./BNK48 Official Website_files/040027qrvwy6.png" alt=""></div>
              </a>
              <br>
              <a href="https://sgo48.vn/" target="_blank">
                <div class="group48Img"><img src="./BNK48 Official Website_files/040038mny129.png" alt=""></div>
              </a>
              <a href="https://www.weibo.com/u/5958250307" target="_blank">
                <div class="group48Img"><img src="./BNK48 Official Website_files/040054klor39.png" alt=""></div>
              </a>
            </div>
          </div>

          <form id="selectMembersForm">
            <div class="modal fade" id="selectMembers" role="dialog">
              <div class="modal-dialog selectMembersBox">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title MemhText">เลือกดูความเคลื่อนไหวของน้องๆ</h4>
                    <button type="button" class="btn btn-default txtTransform MemClear">Clear Selection</button>
                    <button type="button" class="btn btn-default txtTransform MemSelect">Select All</button>
                  </div>
                  <div class="modal-body">
                    <div class="row ImgInstarboxM">
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_52" value="52" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_52">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_52" style="background: url(data/Members/52/s/20181123091728bcfhk5.png) center center;"></div>
                          </div>
                          <div class="nameInstar">AOM</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_51" value="51" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_51">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_51" style="background: url(data/Members/51/s/20181123091846grv159.png) center center;"></div>
                          </div>
                          <div class="nameInstar">BAMBOO</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_50" value="50" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_50">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_50" style="background: url(data/Members/50/s/20181123091902cimrw9.png) center center;"></div>
                          </div>
                          <div class="nameInstar">CAKE</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_4" value="4" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_4">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_4" style="background: url(data/Members/4/s/20181123093609prtvwy.png) center center;"></div>
                          </div>
                          <div class="nameInstar">CHERPRANG</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_49" value="49" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_49">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_49" style="background: url(data/Members/49/s/20181123091918cdkos7.png) center center;"></div>
                          </div>
                          <div class="nameInstar">DEENEE</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_48" value="48" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_48">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_48" style="background: url(data/Members/48/s/20181123091933efw249.png) center center;"></div>
                          </div>
                          <div class="nameInstar">FAII</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_47" value="47" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_47">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_47" style="background: url(data/Members/47/s/20181123091949dw2456.png) center center;"></div>
                          </div>
                          <div class="nameInstar">FIFA</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_46" value="46" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_46">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_46" style="background: url(data/Members/46/s/20181123092006adkv45.png) center center;"></div>
                          </div>
                          <div class="nameInstar">FOND</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_45" value="45" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_45">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_45" style="background: url(data/Members/45/s/20181123092024eimpv1.png) center center;"></div>
                          </div>
                          <div class="nameInstar">GYGEE</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_25" value="25" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_25">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_25" style="background: url(data/Members/25/s/20181123092844eikqx9.png) center center;"></div>
                          </div>
                          <div class="nameInstar">IZURINA</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_23" value="23" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_23">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_23" style="background: url(data/Members/23/s/20181123092921bfjp46.png) center center;"></div>
                          </div>
                          <div class="nameInstar">JAA</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_24" value="24" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_24">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_24" style="background: url(data/Members/24/s/20181123092904lvyz35.png) center center;"></div>
                          </div>
                          <div class="nameInstar">JANE</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_2" value="2" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_2">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_2" style="background: url(data/Members/2/s/20181123093652ceisz1.png) center center;"></div>
                          </div>
                          <div class="nameInstar">JENNIS</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_22" value="22" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_22">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_22" style="background: url(data/Members/22/s/20181123092940adjpx7.png) center center;"></div>
                          </div>
                          <div class="nameInstar">JIB</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_44" value="44" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_44">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_44" style="background: url(data/Members/44/s/20181123092103bgy126.png) center center;"></div>
                          </div>
                          <div class="nameInstar">JUNÉ</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_21" value="21" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_21">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_21" style="background: url(data/Members/21/s/20181123092956ikpt69.png) center center;"></div>
                          </div>
                          <div class="nameInstar">KAEW</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_20" value="20" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_20">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_20" style="background: url(data/Members/20/s/20181123093013lsw348.png) center center;"></div>
                          </div>
                          <div class="nameInstar">KAIMOOK</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_19" value="19" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_19">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_19" style="background: url(data/Members/19/s/20181123093035fhtvz3.png) center center;"></div>
                          </div>
                          <div class="nameInstar">KATE</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_43" value="43" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_43">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_43" style="background: url(data/Members/43/s/20181123092143hloqsu.png) center center;"></div>
                          </div>
                          <div class="nameInstar">KHAMIN</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_42" value="42" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_42">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_42" style="background: url(data/Members/42/s/20181123092218glou89.png) center center;"></div>
                          </div>
                          <div class="nameInstar">KHENG</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_18" value="18" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_18">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_18" style="background: url(data/Members/18/s/20181123093052ckr368.png) center center;"></div>
                          </div>
                          <div class="nameInstar">KORN</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_41" value="41" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_41">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_41" style="background: url(data/Members/41/s/20181123092240bglmt3.png) center center;"></div>
                          </div>
                          <div class="nameInstar">MAIRA</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_40" value="40" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_40">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_40" style="background: url(data/Members/40/s/20181123092304eglsz2.png) center center;"></div>
                          </div>
                          <div class="nameInstar">MEWNICH</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_16" value="16" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_16">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_16" style="background: url(data/Members/16/s/20181123093116fnsw27.png) center center;"></div>
                          </div>
                          <div class="nameInstar">MIND</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_39" value="39" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_39">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_39" style="background: url(data/Members/39/s/20181123092324cdqs15.png) center center;"></div>
                          </div>
                          <div class="nameInstar">MINMIN</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_15" value="15" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_15">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_15" style="background: url(data/Members/15/s/20181123093136abez56.png) center center;"></div>
                          </div>
                          <div class="nameInstar">MIORI</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_14" value="14" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_14">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_14" style="background: url(data/Members/14/s/20181123093154bcsuv9.png) center center;"></div>
                          </div>
                          <div class="nameInstar">MOBILE</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_13" value="13" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_13">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_13" style="background: url(data/Members/13/s/20181123093213adej67.png) center center;"></div>
                          </div>
                          <div class="nameInstar">MUSIC</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_38" value="38" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_38">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_38" style="background: url(data/Members/38/s/20181123092347cfw568.png) center center;"></div>
                          </div>
                          <div class="nameInstar">MYYU</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_12" value="12" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_12">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_12" style="background: url(data/Members/12/s/20181123093240ioxy14.png) center center;"></div>
                          </div>
                          <div class="nameInstar">NAMNEUNG</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_11" value="11" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_11">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_11" style="background: url(data/Members/11/s/20181123093259mqst29.png) center center;"></div>
                          </div>
                          <div class="nameInstar">NAMSAI</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_37" value="37" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_37">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_37" style="background: url(data/Members/37/s/20181123092429htuy28.png) center center;"></div>
                          </div>
                          <div class="nameInstar">NATHERINE</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_36" value="36" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_36">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_36" style="background: url(data/Members/36/s/20181123092449cfilyz.png) center center;"></div>
                          </div>
                          <div class="nameInstar">NEW</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_35" value="35" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_35">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_35" style="background: url(data/Members/35/s/20181123092509cfimns.png) center center;"></div>
                          </div>
                          <div class="nameInstar">NIKY</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_34" value="34" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_34">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_34" style="background: url(data/Members/34/s/20181123092529ekruy5.png) center center;"></div>
                          </div>
                          <div class="nameInstar">NINE</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_10" value="10" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_10">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_10" style="background: url(data/Members/10/s/20181123093355cfpu38.png) center center;"></div>
                          </div>
                          <div class="nameInstar">NINK</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_9" value="9" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_9">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_9" style="background: url(data/Members/9/s/20181123093415cdghm9.png) center center;"></div>
                          </div>
                          <div class="nameInstar">NOEY</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_33" value="33" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_33">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_33" style="background: url(data/Members/33/s/20181123092548fqstv6.png) center center;"></div>
                          </div>
                          <div class="nameInstar">OOM</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_3" value="3" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_3">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_3" style="background: url(data/Members/3/s/20181123093629ghtuv5.png) center center;"></div>
                          </div>
                          <div class="nameInstar">ORN</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_32" value="32" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_32">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_32" style="background: url(data/Members/32/s/20181123092613gos567.png) center center;"></div>
                          </div>
                          <div class="nameInstar">PAKWAN</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_31" value="31" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_31">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_31" style="background: url(data/Members/31/s/20181123092634cdefru.png) center center;"></div>
                          </div>
                          <div class="nameInstar">PANDA</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_30" value="30" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_30">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_30" style="background: url(data/Members/30/s/20181123092657ghptv5.png) center center;"></div>
                          </div>
                          <div class="nameInstar">PHUKKHOM</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_8" value="8" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_8">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_8" style="background: url(data/Members/8/s/20181123093439aimos2.png) center center;"></div>
                          </div>
                          <div class="nameInstar">PIAM</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_1" value="1" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_1">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_1" style="background: url(data/Members/1/s/20181123093714fgmz14.png) center center;"></div>
                          </div>
                          <div class="nameInstar">PUN</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_7" value="7" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_7">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_7" style="background: url(data/Members/7/s/20181123093500kmsx57.png) center center;"></div>
                          </div>
                          <div class="nameInstar">PUPE</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_29" value="29" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_29">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_29" style="background: url(data/Members/29/s/20181123092722ehivw5.png) center center;"></div>
                          </div>
                          <div class="nameInstar">RATAH</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_6" value="6" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_6">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_6" style="background: url(data/Members/6/s/20181123093532acdrs6.png) center center;"></div>
                          </div>
                          <div class="nameInstar">SATCHAN</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_28" value="28" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_28">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_28" style="background: url(data/Members/28/s/20181123092742bdjtu3.png) center center;"></div>
                          </div>
                          <div class="nameInstar">STANG</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_5" value="5" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_5">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_5" style="background: url(data/Members/5/s/20181123093550ceoswy.png) center center;"></div>
                          </div>
                          <div class="nameInstar">TARWAAN</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_27" value="27" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_27">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_27" style="background: url(data/Members/27/s/20181123092802del457.png) center center;"></div>
                          </div>
                          <div class="nameInstar">VIEW</div>
                        </label>
                      </div>
                      <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                        <input type="checkbox" name="nameInstar[]" id="Img_26" value="26" class="nameInstarchk">
                        <label class="ImgInstarLabel" for="Img_26">
                          <div class="ImgInstarbox">
                            <div class="ImgInstar Img_26" style="background: url(data/Members/26/s/20181123092819glnst8.png) center center;"></div>
                          </div>
                          <div class="nameInstar">WEE</div>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer Membersfooter">
                    <button type="button" class="btn btn-default MemDismiss" data-dismiss="modal">ยกเลิก</button>
                    <button type="button" class="btn btn-default MemConf">ตกลง</button>
                  </div>
                </div>
              </div>
            </div>
          </form>


          <div class="modal fade" id="viewdetailschtxt" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content v1">
                <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal">
                    <i class="fa fa-times-circle-o" aria-hidden="true"></i>
                  </button>
                  <div class="row Boxschedulex">
                    <div class="col-md-12">
                      <div class="Boxschedule">
                        <div class="Boxschedule2">
                          <div class="classEven"></div>
                          <div class="classEvendate"></div>
                          <div class="classboxschdate">
                            <div class="boxschdate"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <link rel="stylesheet" href="./BNK48 Official Website_files/home.css">
          <script type="text/javascript" src="./BNK48 Official Website_files/home.js.ดาวน์โหลด"></script>
          <script type="text/javascript">
            new Vue({
              el: '#carousel3d',
              data: {
                slides: 5
              },
              components: {
                'carousel-3d': Carousel3d.Carousel3d,
                'slide': Carousel3d.Slide
              }
            });
          </script>
          <img class="imgbg" src="./BNK48 Official Website_files/group-27.png" alt="">
        </div>
        <div id="footer">
         <div class="footer">
          Copyright © 2018, BNK48 Office. All rights reserved
        </div>
      </div>

      <a id="back_top" href="https://www.bnk48.com/index.php?page=home#" class="btn btn-lg btn_backTop" role="button" data-placement="left" style="display: none;"><i class="fa fa-angle-double-up"></i></a>
    </div>
    

  </body></html>
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/logout','Controllernaja@logout');
Route::get('/edit','Controllernaja@edit');
Route::get('/homes','Controllernaja@homes');
Route::get('/schedule','Controllernaja@schedule');
Route::get('/admin','Controllernaja@admin');
Route::get('/ordering','Controllernaja@ordering');


Route::get('/shop','Controllernaja@shop');
Route::get('/shopdetail','Controllernaja@shopdetail');


Route::get('/ticket','Controllernaja@ticket');
Route::post('/ticket','Controllernaja@ticket');


Route::get('/singin','Controllernaja@singin');
Route::post('/singin','Controllernaja@checkLogin');

Route::get('/shopsingin','Controllernaja@shopsingin');
Route::post('/shopsingin','Controllernaja@checkLogin');

Route::get('/register','Controllernaja@register');
Route::post('/register','Controllernaja@chekregister');

Route::post('/update','Controllernaja@update');



/*
Route::get('/', function () {
    return view('welcome');
});

Route::get('/hw_Document_Size(hw_document)','Controllernaja@homes');
Route::get('/ticket','Controllernaja@ticket');
Route::get('/schedule','Controllernaja@schedule');
Route::get('/register','Controllernaja@register');
Route::get ('/chek_login','Controllernaja@chek_login');
Route::post ('/ticket','Controllernaja@chek_login');




Route::get('/singin','Controllernaja@singin');

Route::post('/login' , 'Usercontroller@checkLogin');

Route::get('/addmin' , 'Usercontroller@addmin');
Route::get('/Register' , 'Usercontroller@Register');
Route::post('/Register' , 'Usercontroller@insert_data');*/